#ifndef NO_TEST

#include "synchro.hpp"
#include "journaling_container.hpp"

#include "test/test.hpp"

#include <fstream>
#include <cstdio>
#include <iostream>

#include <vector>
#include <map>
#include <variant>
#include <limits>
#include <tuple>
//static assertions:
namespace synchro
{

	//////////////////////////////////////////////////////////////
	//Section: Tests for concept definitions made by Synchro
	//////////////////////////////////////////////////////////////

	static_assert(SequenceContainer<std::vector<int>>);
	static_assert(!SequenceContainer<std::map<int,int>>);

	static_assert(AssociativeContainer<std::map<int,int>>);

	static_assert(Variant<std::variant<int,bool,char>>);
	static_assert(!Variant<std::tuple<int,bool,char>>);

	static_assert(Tuple<std::tuple<int>>);
	static_assert(!Tuple<std::variant<int,bool,char>>);

	static_assert(Pair<std::pair<int,int>>);
	static_assert(!Pair<int>); //this happened at one point... never again.


	///
	static_assert(serialization_string_exact_size(char(3))==1);
	///


	//////////////////////////////////////////////////////////////
	//Section: ElementalSerializationType unit tests
	//This essentially ensures that all of these types have the proper
	//functions defined.
	//////////////////////////////////////////////////////////////

	static_assert(!ElementalSerializationType<void>);
	static_assert(ElementalSerializationType<int>);
	static_assert(ElementalSerializationType<int&>);
	static_assert(ElementalSerializationType<int const &>);
	static_assert(ElementalSerializationType<int &&>);
	static_assert(ElementalSerializationType<std::vector<int>>);
	static_assert(ElementalSerializationType<std::string>);
	//static_assert(ElementalSerializationType<std::array<int,3>>); //TODO
	static_assert(ElementalSerializationType<std::tuple<int>>);
	static_assert(ElementalSerializationType<std::tuple<int,float,std::vector<int>,std::pair<float,int>>>);
	static_assert(ElementalSerializationType<std::map<int,int>>);
	static_assert(ElementalSerializationType<std::optional<int>>);
	static_assert(ElementalSerializationType<std::variant<int,float>>);
	static_assert(ElementalSerializationType<std::monostate>);



};






class const_test
{
	public:
		int y;
		int const x;

		using enumeration = synchro::enumeration<&const_test::y,&const_test::x>;
};
//static_assert(synchro::is_member_function_pointer_to_const_v<decltype(&const_test::x)>);

class int_class
{
	public:
		int x; 

		/*constexpr int_class(int _x) : x(_x) {}
		constexpr int_class() {}
		constexpr int_class(synchro::Snapshot auto const & snap)// :
		//	x(synchro::get<&int_class::x>(snap))
		{
			printf("INTCLASS %s\n", typeid(decltype(snap)).name());
		}*/

		using enumeration = synchro::enumeration<&int_class::x>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

		std::string unit_test_to_string() const { return "{"+std::to_string(x)+"}"; }

		bool operator==(int_class const & other) const { return x == other.x; }
};
//static_assert(std::is_same_v<synchro::remove_snapshot_t<synchro::initial_snapshot<int_class>>, int_class>);
//static_assert(std::is_same_v<synchro::remove_snapshot_t<std::vector<synchro::initial_snapshot<int_class>>>, std::vector<int_class>>);

struct int_class_class
{
	int_class c;

	using enumeration = synchro::enumeration<&int_class_class::c>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

	std::string unit_test_to_string() const { return c.unit_test_to_string(); }

	bool operator==(int_class_class const & other) const { return c == other.c; }
};
static_assert(std::is_standard_layout_v<int_class_class>);

struct int64_class
{
	std::uint64_t x;
	using enumeration = synchro::enumeration<&int64_class::x>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};



struct float_class
{
	float x;
	double y;
	float z;
	float w;

	using enumeration = synchro::enumeration<&float_class::x, &float_class::y,&float_class::z,&float_class::w>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

	bool operator==(float_class const & other) const { return x == other.x && y == other.y; }
};

struct embed_sync_test
{
	synchro::initial_snapshot<int_class> snapshot;

	using enumeration = synchro::enumeration<&embed_sync_test::snapshot>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

	embed_sync_test(synchro::Snapshot auto const & in) : 
		snapshot(synchro::get<&embed_sync_test::snapshot>(in))
	{
	}
	embed_sync_test(synchro::initial_snapshot<int_class> in) :
		snapshot(in)
	{
	}
};
struct embed_sync_vec_test
{
	std::vector<synchro::initial_snapshot<int_class>> snapshot_vec;

	using enumeration = synchro::enumeration<&embed_sync_vec_test::snapshot_vec>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

};

class encap_test
{
	public:
		int x = 28;
		int_class encap;

		using enumeration = synchro::enumeration<&encap_test::x, &encap_test::encap>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};
//static_assert( synchro::SerializationStringExactSizeCompileTimeKnowable< synchro::resync_snapshot<encap_test> >);

class container_test
{
	public:
		std::vector<int> vec;

		using enumeration = synchro::enumeration<&container_test::vec>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};
class nontriv_container_test
{
	public:
		std::vector<int_class> vec;

		using enumeration = synchro::enumeration<&nontriv_container_test::vec>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};
class three_deep_container_test
{
	public:
		std::vector<int_class_class> vec;

		using enumeration = synchro::enumeration<&three_deep_container_test::vec>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

//static_assert( !synchro::SerializationStringExactSizeCompileTimeKnowable< synchro::resync_snapshot<container_test> >);

class int_class_container
{
	public:
		std::map<size_t,int_class> s;

		using enumeration = synchro::enumeration<&int_class_container::s>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};
class private_test
{
	int x;
	public:
		private_test(int in) : x(in) {}

		void reset_x() { x = 0; }

		using enumeration = synchro::enumeration<&private_test::x>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();

		std::string unit_test_to_string() const { return "{"+std::to_string(x)+"}"; }

		bool operator==(private_test const & other) const { return x == other.x; }
};

class map_test
{
	public:
		std::map<int,int> map;
		using enumeration = synchro::enumeration<&map_test::map>;

		void operator=(synchro::Snapshot auto in)
		{
			in.apply(this);
		}
};
struct str_test
{
	std::string str;
	using enumeration = synchro::enumeration<&str_test::str>;
	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

class tuple_test
{
	public:
		std::tuple<int, float> t;

		using enumeration = synchro::enumeration<&tuple_test::t>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

class variant_test
{
	public:
		std::variant<int,float, int_class> v;

		using enumeration = synchro::enumeration<&variant_test::v>;

		SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

struct my_global_info
{
	int32_t global_int;
};
struct global_test
{
	int32_t global_int_value;
	int32_t local_int;

	using enumeration = synchro::enumeration<&global_test::local_int>;

	void operator=(synchro::Snapshot auto in)
	{
		global_int_value = in.global_info.global_int;
		in.apply(this);
	}
};

struct empty_enum
{
	using enumeration = synchro::enumeration<>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

struct func_test
{
	int x;

	int savex() const
	{
		return x+1;
	}
	void loadx(int new_x)
	{
		x = new_x-1;
	}

	using enumeration = synchro::enumeration<
		synchro::sync_with_functions<&func_test::savex, &func_test::loadx>{}
	>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};

//non trivial function test
struct ntfunc
{
	int x;

	//return a class which must be snapshotted
	int_class savex() const
	{
		return {x+1};
	}
	void loadx(int_class new_x)
	{
		x = new_x.x-1;
	}

	using enumeration = synchro::enumeration<
		synchro::sync_with_functions<&func_test::savex, &func_test::loadx>{}
	>;

	SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
};


//not syncable classes

class cant_use_ptr
{
	public:
		int* ptr;
		using enumeration = synchro::enumeration<&cant_use_ptr::ptr>;
};
class must_have_enumeration
{
	public:
		int data;
};



void qwrite(std::string fname, std::string buffer)
{
	std::ofstream out_stream;
	out_stream.open(fname);
	out_stream << buffer;
	out_stream.close();
}
std::string qread(std::string fname)
{
	std::ifstream in_stream;
	in_stream.open(fname);
	std::stringstream result;
	result << in_stream.rdbuf();
	in_stream.close();

	return result.str();
}
template<typename T>
T load(std::string path)
{
	auto tape = synchro::serialization_tape{};
	tape.data = filesystem::read_all_from_file(path);
	auto snapshot = synchro::initial_snapshot<T>(tape);
	T result;
	result = snapshot;
	return result;
}
template<typename T>
void save(std::string path, T in)
{
	auto snapshot = synchro::initial_snapshot<T>{in};
	auto tape = synchro::serialization_tape{snapshot};
	filesystem::write_to_file(path, tape.data);
}

namespace synchro
{
	void unit_test(test::instance& inst)
	{
		inst.set_name("Synchro");

		srand(time(NULL));
		{
			int_class obj;

			int rand_val = rand() % 1000;
			obj.x = rand_val;

			synchro::resync_snapshot<decltype(obj)> mydata{obj};

			obj.x = rand_val+1;

			obj = mydata;

			inst.inform("Ensure snapshots work for trivial types");
			inst.test(TEST_2(obj.x,rand_val,obj.x==rand_val,true));

			obj.x = rand_val+1;

			auto tape = synchro::serialization_tape(mydata);

			tape.reset_cursor();

			auto snapshot_from_tape = synchro::resync_snapshot<int_class>(tape);

			obj = snapshot_from_tape;

			inst.inform("Ensure serialization works for trivial types");
			inst.test(TEST_2(obj.x,rand_val,obj.x==rand_val,true));

		}
		{
			int_class obj;

			int rand_val = rand() % 1000;
			obj.x = rand_val;

			synchro::initial_snapshot<decltype(obj)> mydata{obj};

			embed_sync_test embed_obj = {obj};

			synchro::initial_snapshot<embed_sync_test> embed_data{embed_obj};

			embed_sync_test out_obj{embed_data};

			synchro::initial_snapshot<int_class> out_data = out_obj.snapshot;

			obj.x = rand_val+1;

			obj = out_data;

			inst.inform("Ensure embedded snapshots work");
			inst.test(TEST_2(obj.x,rand_val,obj.x==rand_val,true));

			obj.x = rand_val+1;

			auto tape = synchro::serialization_tape(embed_data);

			tape.reset_cursor();

			auto snapshot_from_tape = synchro::initial_snapshot<embed_sync_test>(tape);

			embed_sync_test out_from_tape_obj{snapshot_from_tape};

			obj = out_from_tape_obj.snapshot;

			inst.inform("Ensure serialization works for embedded snapshots");
			inst.test(TEST_2(obj.x,rand_val,obj.x==rand_val,true));

		}
		{
			int_class obj0 = {1};
			int_class obj1 = {5};
			int_class obj2 = {99};
			
			auto data0 = synchro::initial_snapshot<int_class>{obj0};
			auto data1 = synchro::initial_snapshot<int_class>{obj1};
			auto data2 = synchro::initial_snapshot<int_class>{obj2};

			embed_sync_vec_test etest = {{data0,data1,data2}};

			auto data = synchro::initial_snapshot<embed_sync_vec_test>{etest};

			embed_sync_vec_test out;
			out = data;

			int_class out_obj0;
			out_obj0 = out.snapshot_vec[0];
			int_class out_obj1;
			out_obj1 = out.snapshot_vec[1];
			int_class out_obj2;
			out_obj2 = out.snapshot_vec[2];

			inst.inform("Ensure serialization works for embedded snapshot vectors");
			inst.test(TEST_2(obj0.x,out_obj0.x,obj0.x==out_obj0.x,true));
			inst.test(TEST_2(obj1.x,out_obj1.x,obj1.x==out_obj1.x,true));
			inst.test(TEST_2(obj2.x,out_obj2.x,obj2.x==out_obj2.x,true));
		}
		{
			int64_class i0 = {34785893475893457};

			auto data0 = synchro::initial_snapshot<int64_class>{i0};

			int64_class i1;
			i1 = data0;

			inst.inform("Ensure snapshotting int64 works");
			inst.test(TEST_2(i0.x,i1.x,i0.x==i1.x,true));

			auto tape = synchro::serialization_tape(data0);
			tape.current_cursor = 0;
			auto data1 = synchro::initial_snapshot<int64_class>{tape};

			int64_class i2;
			i2 = data1;

			inst.inform("Ensure read/write int64 to tapes works");
			inst.test(TEST_2(i0.x,i2.x,i0.x==i2.x,true));
		}
		{
			float_class const f0 = {999.f, std::numeric_limits<double>::max(), 420.f,69.420f};

			auto data0 = synchro::initial_snapshot<float_class>{f0};

			float_class f1;
			f1 = data0;

			inst.inform("Ensure snapshotting floating point types works");
			inst.test(TEST_2(f0.x,f1.x, f0.x==f1.x,true));
			inst.test(TEST_2(f0.y,f1.y, f0.y==f1.y,true));
			inst.test(TEST_2(f0.z,f1.z, f0.z==f1.z,true));
			inst.test(TEST_2(f0.w,f1.w, f0.w==f1.w,true));

			auto tape = synchro::serialization_tape(data0);
			inst.inform("Ensure proper tape size");
			inst.test(TEST_1(tape.data.size(), tape.data.size() == 20, true));
			tape.current_cursor = 0;
			auto data1 = synchro::initial_snapshot<float_class>{tape};
			
			float_class f2;
			f2 = data1;

			inst.inform("Ensure read/writing floating points to tapes works");
			inst.test(TEST_2(f0.x,f2.x, f0.x==f2.x,true));
			inst.test(TEST_2(f0.y,f2.y, f0.y==f2.y,true));
			inst.test(TEST_2(f0.z,f2.z, f0.z==f2.z,true));
			inst.test(TEST_2(f0.w,f2.w, f0.w==f2.w,true));
		}
		{

			encap_test encap;

			encap.encap.x = 8;

			synchro::resync_snapshot<encap_test> encap_data = {encap};
			
			encap_test result;
			result = encap_data;

			inst.inform("Ensure snapshots work for types containing other non-trivial types");
			inst.test(TEST_1(result.encap.x,result.encap.x==8,true));
		}
		{
			container_test ctn;

			ctn.vec.emplace_back(-1);
			ctn.vec.emplace_back(0);
			ctn.vec.emplace_back(50);
			ctn.vec.emplace_back(100);
			ctn.vec.emplace_back(200);
			ctn.vec.emplace_back(300);
			ctn.vec.emplace_back(400);

			auto snapshotted_vector = ctn.vec;

			synchro::initial_snapshot<container_test> ctn_data = {ctn};

			ctn.vec.clear();

			ctn = ctn_data;
			//TODO seems to copy abvout exactly half for some reason...


			inst.inform("Ensure snapshots work for container types holding trivial data");
			inst.test(TEST_2(ctn.vec,snapshotted_vector,ctn.vec==snapshotted_vector,true));

			save("/tmp/unit-test-snapshot-ct.dat", ctn);

			auto loaded = load<container_test>("/tmp/unit-test-snapshot-ct.dat");

			inst.inform("Ensure serialization tapes work for container types holding trivial data");
			inst.test(TEST_2(ctn.vec,loaded.vec,ctn.vec==loaded.vec,true));
		}
		{
			nontriv_container_test ctn;

			ctn.vec.emplace_back(-1);
			ctn.vec.emplace_back(0);
			ctn.vec.emplace_back(50);
			ctn.vec.emplace_back(100);
			ctn.vec.emplace_back(200);
			ctn.vec.emplace_back(300);
			ctn.vec.emplace_back(400);

			auto snapshotted_vector = ctn.vec;

			synchro::initial_snapshot<nontriv_container_test> ctn_data = {ctn};

			ctn.vec.clear();

			ctn = ctn_data;
			//TODO seems to copy abvout exactly half for some reason...


			inst.inform("Ensure snapshots work for container types holding NON-trivial data");
			inst.test(TEST_2(ctn.vec,snapshotted_vector,ctn.vec==snapshotted_vector,true));

			save("/tmp/unit-test-snapshot-ntct.dat", ctn);

			auto loaded = load<nontriv_container_test>("/tmp/unit-test-snapshot-ntct.dat");

			inst.inform("Ensure serialization tapes work for container types holding NON-trivial data");
			inst.test(TEST_2(ctn.vec,loaded.vec,ctn.vec==loaded.vec,true));

		}
		{
			three_deep_container_test ctn;

			ctn.vec.emplace_back(int_class{-1});
			ctn.vec.emplace_back(int_class{0});
			ctn.vec.emplace_back(int_class{50});
			ctn.vec.emplace_back(int_class{100});
			ctn.vec.emplace_back(int_class{200});
			ctn.vec.emplace_back(int_class{300});
			ctn.vec.emplace_back(int_class{400});

			auto snapshotted_vector = ctn.vec;

			synchro::resync_snapshot<three_deep_container_test> ctn_data = {ctn};

			ctn.vec.clear();

			ctn = ctn_data;
			//TODO seems to copy abvout exactly half for some reason...

			inst.inform("Ensure 3-deep containers work");
			inst.test(TEST_2(ctn.vec,snapshotted_vector,ctn.vec==snapshotted_vector,true));

			save("/tmp/unit-test-snapshot-3ct.dat", ctn);

			auto loaded = load<three_deep_container_test>("/tmp/unit-test-snapshot-3ct.dat");

			inst.inform("Ensure serialization tapes work for container types holding 3-deep data");
			inst.test(TEST_2(ctn.vec,loaded.vec,ctn.vec==loaded.vec,true));

		}
		{
			str_test stest = {"test str :)"};

			synchro::resync_snapshot<str_test> stest_data = {stest};

			stest.str.clear();

			stest = stest_data;

			inst.inform("Ensure snapshotting strings works");
			inst.test(TEST_1(stest.str,stest.str=="test str :)",true));
		}
		{
			int_class_container sctn;

			sctn.s.insert(sctn.s.end(),{1,{-1}});
			sctn.s.insert(sctn.s.end(),{2,{3}});
			sctn.s.insert(sctn.s.end(),{3,{500}});

			auto snapshotted_map = sctn.s;

			synchro::resync_snapshot<int_class_container> sctn_data = {sctn};

			sctn.s.clear();

			sctn = sctn_data;

			inst.inform("Ensure snapshots work for container types holding non-trivial data");
			inst.test(TEST_2(sctn.s,snapshotted_map,sctn.s==snapshotted_map,true));
		}
		{
			private_test test = {8};
			private_test original_test = {8};

			auto data = synchro::resync_snapshot<private_test>{test};

			test.reset_x();

			test = data;

			inst.inform("Ensure syncing private member variables works");
			inst.test(TEST_2(test,original_test,test==original_test,true)); 
		}
		//TODO: map test
		{
			tuple_test ttest = {{1, 3.14f}};

			auto data = synchro::resync_snapshot<tuple_test>{ttest};

			ttest.t = {0,0};

			ttest = data;

			inst.inform("Ensure syncing tuples works");
			inst.test(TEST_1(std::get<0>(ttest.t), std::get<0>(ttest.t) == 1,true));
			inst.test(TEST_1(std::get<1>(ttest.t), std::get<1>(ttest.t) == 3.14f,true));
		}
		{
			variant_test vtest = {2};

			auto data = synchro::resync_snapshot<variant_test>{vtest};

			variant_test rtest = {5.0f};
			rtest = data;

			inst.inform("Ensure syncing variants works");
			inst.test(TEST_1(std::get<int>(rtest.v),std::get<int>(rtest.v) == 2, true));

			vtest = {int_class{99}};
			
			auto data2 = synchro::resync_snapshot<variant_test>{vtest};

			rtest = data2;
			inst.inform("Ensure syncing variants with complex data types works");
			inst.test(TEST_1(std::get<int_class>(rtest.v).x,std::get<int_class>(rtest.v).x == 99, true));
		}
		{
			my_global_info glob = {1337};

			global_test gtest = {1,2};
			
			auto data = synchro::resync_snapshot<global_test, my_global_info>{gtest,glob};

			gtest = data;

			inst.inform("Ensure global info works");
			inst.test(TEST_2(gtest.global_int_value, glob.global_int, gtest.global_int_value == glob.global_int,true));
		}
		{
			auto etest = empty_enum{};

			auto data = synchro::resync_snapshot<empty_enum>{etest};

			etest = data;
			//nothing really to assert here, it's more of a test of "will this compile?"
		}
		{
			auto ftest = func_test{99};
			
			auto data = synchro::resync_snapshot<func_test>{ftest};

			ftest.x = 0;

			ftest = data;

			inst.inform("Ensure sync with functions works");
			inst.test(TEST_1(ftest.x, ftest.x == 99,true));
		}
		{
			auto ftest = func_test{99};
			
			auto data = synchro::resync_snapshot<func_test>{ftest};

			ftest.x = 0;

			ftest = data;

			inst.inform("Ensure non-trivial sync with functions works");
			inst.test(TEST_1(ftest.x, ftest.x == 99,true));
		}
		{
			journaling_container<std::monostate, std::map,int,int> jtest;

			jtest.insert(std::pair<int,int>(1,100));

			jtest.commit();

			auto data = synchro::initial_snapshot<decltype(jtest)>{jtest};
			//printf("%s\n", typeid(synchro::initial_snapshot<decltype(jtest)>::version_id_tuple_t).name());
			

			journaling_container<std::monostate, std::map,int,int> rtest;
			rtest = data;


			inst.inform("Ensure synchronizing journaling containers works");
			inst.test(TEST_1(rtest.at(1), rtest.at(1) == 100,true));

			jtest[1] = 120;

			auto data1 = synchro::resync_snapshot<decltype(jtest)>{jtest, {}, data.version_id_tuple};

			rtest = data1;

			inst.test(TEST_1(rtest.at(1), rtest.at(1) == 100,true)); //shouldn't change, we haven't committed anything.

			jtest.commit();

			auto data2 = synchro::resync_snapshot<decltype(jtest)>{jtest, {}, data1.version_id_tuple};

			rtest = data2;
			inst.test(TEST_1(rtest.at(1), rtest.at(1) == 120,true));


			journaling_container<std::monostate, std::map,std::string,int> jteststr;

			jteststr.insert(std::pair<std::string,int>("hi",1));

			jteststr.commit();

			journaling_container<std::monostate, std::map,size_t,int> jtestsizet;
			jtestsizet.insert(std::pair<size_t,int>(1,1));
			jtestsizet.commit();


		}



		//keep this last for convient reading (since it must be manually verified)
		{
			inst.inform("Ensure error reporting works");

			inst.inform(synchro::get_snapshottability_diagnostic<container_test>());
			inst.inform(synchro::get_snapshottability_diagnostic<cant_use_ptr>());
			inst.inform(synchro::get_snapshottability_diagnostic<must_have_enumeration>());
		}

		/*

		*
		auto readresult = qread("/tmp/SERTEST");

		synchro::serialization_tape d_dat;

		d_dat.current_cursor = 0;
		d_dat.data = readresult;

		auto d_rsync_data = synchro::resync_snapshot<tesobj>(d_dat);

		obj = d_rsync_data;

		printf("after deser update: x %d y %d\n",obj.x,obj.y);

		qwrite("/tmp/SERTEST",s_dat.data);
		*
		//////////////////

		encap_test encap;
		printf("before encap %d\n",encap.encap.x);

		synchro::resync_snapshot<encap_test> encap_data = {&encap};
		std::get<0>(std::get<1>(encap_data.data).data) = 6970;
		
		encap = encap_data;

		printf("after encap %d\n",encap.encap.x);

		printf("triv %d\n", std::is_trivially_copyable_v<encap_test>);

		printf("sers size %lu\n",synchro::serialization_string_min_size<decltype(encap_data)>());

		auto ser = synchro::serialization_tape(encap_data);
		//printf("tape size %d\n",ser.data.size());
		ser.reset_cursor();

		auto encap_data_ser = synchro::resync_snapshot<encap_test>(ser);
		encap = encap_data_ser;


		printf("after encap ser %d\n",encap.encap.x);

		//
		
		container_test ctn;

		ctn.vec.push_back(-1);
		ctn.vec.push_back(0);
		ctn.vec.push_back(50);
		ctn.vec.push_back(100);
		ctn.vec.push_back(100);
		ctn.vec.push_back(100);

		printf("vec ser size %lu\n",synchro::serialization_string_exact_size(ctn.vec));

		synchro::resync_snapshot<container_test> ctn_data = {&ctn};
		//printf("req %d \n",synchro::FunctionTemplatableOn<synchro::serialization_string_exact_size, std::vector<int>>);
		printf("req %d \n",synchro::SerializationStringExactSizeCompileTimeKnowable<int>);
		auto ctn_tape = synchro::serialization_tape(ctn_data);

		ctn_tape.reset_cursor();
		//ctn_tape.max_alloc = 24;

		synchro::resync_snapshot<container_test> ctn_deser = {ctn_tape};

		ctn.vec[0] = 999;
		ctn = ctn_deser;

		for (auto& thing : ctn.vec)
		{
			printf("%d\n",thing);
		}

		//
		
		map_test mp;

		mp.map.insert(mp.map.end(),{2,3});
		mp.map.insert(mp.map.end(),{5,7});
		mp.map.insert(mp.map.end(),{69,420});
		mp.map.insert(mp.map.end(),{13,666});

		synchro::resync_snapshot<map_test> mp_data = {&mp};

		auto mp_tape = synchro::serialization_tape(mp_data);

		mp_tape.reset_cursor();

		synchro::resync_snapshot<map_test> mp_deser = {mp_tape};

		mp.map.clear();

		mp = mp_deser;

		for(auto& pair : mp.map)
		{
			printf("%d : %d\n",pair.first,pair.second);
		}

		variant_test x;
		x.v = 6.5f;

		synchro::resync_snapshot<variant_test> myx_data = {&x};

		auto myx_tape = synchro::serialization_tape(myx_data);

		std::cout << "siz " << myx_tape.data.size() << std::endl;

		myx_tape.reset_cursor();

		synchro::resync_snapshot<variant_test> myx_deser = {myx_tape};

		x.v = 7.f;

		x = myx_deser;

		std::cout << "final " << std::get<float>(x.v) << std::endl;

		*/
	}
}

#endif //ifndef NO_TEST
