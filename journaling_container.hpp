/*!
 * Wrapper for containers which journals changes, for more effecient resyncing.
 */
#pragma once

#include "synchro/enumeration.hpp"
#include "synchro/synchro.hpp"
#include "algorithm/type_traits.hpp"

#include <vector>
#include <unordered_map>

namespace synchro
{
	struct commit_specifies_nonexistant_iterator : public faulty_snapshot
	{
		inline commit_specifies_nonexistant_iterator() :
			faulty_snapshot("Container specifies a non-existant iterator in the commit data for either update or insert vectors.")
		{
		}
	};
	struct commit_specifies_nonexistant_key : public faulty_snapshot
	{
		inline commit_specifies_nonexistant_key() :
			faulty_snapshot("Container specifies a non-existant key in the underlying container.")
		{
		}
	};
	auto get_vec_at_untrusted_it(auto & vec, auto it)
	{
		if (it >= vec.size()) [[unlikely]]
		{
			throw commit_specifies_nonexistant_iterator{};
		}
		return vec[it];
	}

	template<typename global_info_t, template<typename ...> typename underlying_container_template, typename ... args>
	class journaling_container
	{
		using underlying_container_t = underlying_container_template<args...>;
		underlying_container_t underlying_container;

		using key_type = typename underlying_container_t::key_type;
		using mapped_type = typename underlying_container_t::mapped_type;

		using value_type = std::pair<key_type,mapped_type>;
		struct saveable_value_type
		{
			mapped_type value;
			using enumeration = synchro::enumeration<&saveable_value_type::value>;

			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		};
		//using saveable_value_type = value_type;

		/*!
		 * commit_id_t is simply an ID number which points to a commit in commit_id_t::kept_journal
		 *
		 * The reason that this is a struct and not an alias is to make sure that one `journaling_container` of different template parameters' 
		 * commit ID's are incompatible with another.
		 *
		 * This can come into play when there are multiple types of `journaling_container`s in a std::optional being snapshotted, and thus their version_id_t(which will be commit_id_t) would
		 * be the same if an alias was used. This would cause errors.
		 *
		 * Thus, making this a struct results in all of them being seperate types which avoids compile time & run time errors.
		 */
		//TODO: include the contained's version_id_tuple
		struct commit_id_t
		{
			size_t id;
		};

		enum class change_type : unsigned char
		{
			insert,
			update,
			erase
		};
		struct insert_iterator
		{
			key_type key;
			size_t it;
			using enumeration = synchro::enumeration<&insert_iterator::it, &insert_iterator::key>;
			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		};
		struct update_iterator
		{
			key_type key;
			size_t it;
			using enumeration = synchro::enumeration<&update_iterator::it, &update_iterator::key>;
			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		};
		struct erase_key
		{
			key_type key;
			using enumeration = synchro::enumeration<&erase_key::key>;
			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		};
		using change_t = std::variant<insert_iterator, update_iterator, erase_key>;

		//note: assumes that the item updated currently exists in underlying_container
		void insert_to_update_list(auto const & key)
		{
			if (staging_journal.contains(key) && staging_journal.at(key) == change_type::insert)
			{ //leave it as insert type, we still need to capture the const-qualified contents.
			}
			else
			{
				staging_journal[key] = change_type::update;
			}
		}

		using resync_t = synchro::resync_snapshot<saveable_value_type>;
		using initial_sync_t = synchro::initial_snapshot<saveable_value_type>;


		//where changes are held until commit() is called.
		std::unordered_map<key_type,change_type> staging_journal;

		struct commit_history_t
		{
			std::vector<change_t> kept_journal;
			std::vector<initial_sync_t> insert_list;
			std::vector<resync_t> update_list;

			using enumeration = synchro::enumeration
			<
				&commit_history_t::kept_journal,
				&commit_history_t::update_list,
				&commit_history_t::insert_list
			>;

			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();
		} commit_history;

		
		commit_history_t save(global_info_t global_info, commit_id_t last_commit_in, commit_id_t & this_commit_out) const
		{
			printf("last commit %lu\n", last_commit_in.id);
			auto&& out = commit_history_t{};

			out.kept_journal.insert(out.kept_journal.end(), commit_history.kept_journal.begin() + last_commit_in.id, commit_history.kept_journal.end());
			for (change_t change : out.kept_journal)
			{
				std::visit([this,&out]<typename this_change_iterator_t>(this_change_iterator_t this_change_iterator)
				{
					if constexpr(std::is_same_v<insert_iterator,this_change_iterator_t>)
					{
						out.insert_list.push_back(commit_history.insert_list[this_change_iterator.it]);
					}
					else if constexpr(std::is_same_v<update_iterator, this_change_iterator_t>)
					{
						out.update_list.push_back(commit_history.update_list[this_change_iterator.it]);
					}
				}, change);
			}
			this_commit_out = {commit_history.kept_journal.size()};
			printf("out commit %lu\n", this_commit_out.id);

			return std::move(out);
		}
		void load(commit_history_t new_history)
		{
			commit_history.kept_journal.insert(commit_history.kept_journal.end(), new_history.kept_journal.begin(), new_history.kept_journal.end());
			commit_history.update_list.insert(commit_history.update_list.end(), new_history.update_list.begin(), new_history.update_list.end());
			commit_history.insert_list.insert(commit_history.insert_list.end(), new_history.insert_list.begin(), new_history.insert_list.end());

			for (change_t this_change : new_history.kept_journal)
			{
				std::visit([this]<typename this_change_iterator_t>(this_change_iterator_t this_change_iterator)
				{
					if constexpr(std::is_same_v<insert_iterator,this_change_iterator_t>)
					{
					//TODO exception handling
						underlying_container.insert( underlying_container.end(), 
								std::make_pair(this_change_iterator.key, construct_from_snapshot<saveable_value_type>(get_vec_at_untrusted_it(commit_history.insert_list,this_change_iterator.it)).value)
						);
						return;
					}
					auto it = underlying_container.find(this_change_iterator.key);
					if (it == underlying_container.end())
					{
						throw commit_specifies_nonexistant_key{};
					}

					if constexpr(std::is_same_v<update_iterator, this_change_iterator_t>)
					{
						it->second = construct_from_snapshot<saveable_value_type>(get_vec_at_untrusted_it(commit_history.update_list,this_change_iterator.it)).value;
					}
					else //erase
					{
						underlying_container.erase(it);
					}
				}, this_change);
			}
		}
		public:

			using enumeration = synchro::enumeration
			<
				synchro::sync_with_functions<&journaling_container::save, &journaling_container::load, commit_id_t>{}
			>;
			SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT();




			//TODO: More complete STL-like implementation

			auto insert(value_type const & value)
			{
				staging_journal[value.first] = change_type::insert;
				return underlying_container.insert(value);
			}


			//Accesses the element, or if it doesn't exist, creates it as default-constructed.
			mapped_type& operator[](key_type const & key)
				requires(std::default_initializable<mapped_type>)
			{
				auto returnval_it = underlying_container.find(key);
				if (returnval_it == underlying_container.end())
				{
					returnval_it = insert(value_type(key, {})).first;
					staging_journal[key] = change_type::insert;
				}
				else
				{
					insert_to_update_list(key);
				}
				return returnval_it->second;
			}
			mapped_type& at(key_type const & key)
			{
				//may throw exception:
				mapped_type& returnval = underlying_container.at(key);

				insert_to_update_list(key);
				return returnval;
			}

			mapped_type const & at(key_type const & key) const
			{
				return underlying_container.at(key);
			}
			//Always uses the const version of `at` to avoid unneccesary updates in the commit where no changes
			//to the object at `key` where actually made.
			mapped_type const & at_const(key_type const & key) const
			{
				return at(key); //^^^
			}


			void commit()
			{
				//move things from the staging journal into the kept_journal.
				for (auto [key,this_change_type] : staging_journal)
				{
					switch(this_change_type)
					{
						case change_type::insert:
							commit_history.kept_journal.emplace_back(insert_iterator{key, commit_history.insert_list.size()});
							commit_history.insert_list.emplace_back(initial_sync_t{saveable_value_type{underlying_container[key]}});
							break;
						case change_type::update:
							commit_history.kept_journal.emplace_back(update_iterator{key, commit_history.update_list.size()});
							commit_history.update_list.emplace_back(resync_t{saveable_value_type{underlying_container[key]}});
							break;
						case change_type::erase:
							commit_history.kept_journal.emplace_back(erase_key{key});
							break;
					}
				}
				staging_journal.clear();
			}
	};

}
