#pragma once

#include <tuple>
#include <type_traits>
#include <string>
#include <variant>

#include "variadic_util/variadic_util.hpp"
#include "algorithm/type_traits.hpp"

namespace synchro
{
	

	//////////////////////////////////////////////////////////////
	//Section: Synchro concepts definitions
	//////////////////////////////////////////////////////////////
	
	//Base classes for "tagging" purposes.
	struct snapshot_base_class {};
	struct enumeration_base_class {};
	class sync_flag {};
	
	template<typename T>
	concept Snapshot = std::is_base_of_v<snapshot_base_class, T>;

	template<typename T, typename this_snapshot_t>
	concept SnapshotOf = Snapshot<this_snapshot_t> && requires()
	{
		std::is_same_v<T, typename this_snapshot_t::object_t>;
	};

	template<typename T>
	concept SyncFlag = std::is_base_of_v<sync_flag, T>;
	template<typename T>
	concept has_snapshot_enumeration = requires()
	{
		typename T::enumeration;
		std::is_base_of_v<enumeration_base_class, typename T::enumeration>;
	};

	//////////////////////////////////////////////////////////////
	//Section: General concepts definitions
	//////////////////////////////////////////////////////////////

	template<typename T>
	concept SequenceContainer = !has_snapshot_enumeration<T> && requires(T t, typename T::value_type value)
	{
		typename T::value_type;
		{ t.size() } -> std::same_as<size_t>;
		{ t.push_back(value) };
		{ !requires {{T::key_type};} };
	};

	template<typename T>
	concept AssociativeContainer = !has_snapshot_enumeration<T> && requires(T t, std::pair<typename T::key_type,typename T::mapped_type> pair, typename T::iterator it)
	{
		typename T::value_type;
		{ t.size() } -> std::same_as<size_t>;
		{ t.insert(it, pair) };
	};

	template<typename T>
	concept Container = SequenceContainer<T> || AssociativeContainer<T>;

	template<typename T>
	concept Reservable = requires(T t)
	{
		{ t.reserve(size_t(0)) };
	};

	template<typename T>
	concept Optional = !has_snapshot_enumeration<T> && requires(T t)
	{
		{ t.has_value() } -> std::same_as<bool>;
		{ t.value() } -> std::convertible_to<typename T::value_type>;
	};
	
	template<typename T>
	concept Variant = !has_snapshot_enumeration<T> && requires(T t)
	{
		{ t.index() } -> std::same_as<size_t>;
		//TODO: This causes problems for some reason and I don't understand why 
		//{ get<size_t{0}>(t) }; //OK: all variants must be at least of size 1.
	};

	//Can't be defined with a c++20 concept, so c++11->20 wrapper.
	
	/*
	template<template<typename T ...> typename, std::tuple<T...> Y>
	struct impl_tuple
	{
		constexpr static bool value = true;
	}
	*/
	template<typename>
	struct impl_tuple
	{
		constexpr static bool value = false;
	};
	template<typename ... T>
	struct impl_tuple<std::tuple<T...>>
	{
		constexpr static bool value = true;
	};
	template<typename ... T>
	struct impl_tuple<std::pair<T...>>
	{
		constexpr static bool value = true;
	};
	template<typename T>
	concept Tuple = impl_tuple<T>::value;
	static_assert(Tuple<std::tuple<int8_t, size_t,std::string>>);
	static_assert(Tuple<std::pair<int8_t, std::string>>);

	//Unfortunately this cannot be removed because it demonstrates parenthesis around requires in a way that I like
	/*
	//TODO: Couldn't I just test if tuple_cat works and call it a day?
	template<typename T> 
	concept Tuple = !has_snapshot_enumeration<T> && (requires(T t)
	{
		typename std::tuple_size<T>;
		//tuple_cat to support 0-size tuples and `false` just cuz
		typename std::tuple_element<0,decltype(std::tuple_cat(std::make_tuple(false),t))>;
		{ std::get<0>(std::tuple_cat(std::make_tuple(false),t)) };
		requires (!Variant<T>);
		requires (!SequenceContainer<T>);
		//The problem is tha tuple_cat with anything will create an implicit tuple.
		//We need a way to sort what's a tuple based not upon the '0'.
		requires (!std::is_enum_v<T>);
	}) || std::same_as<T,std::tuple<>>;
	*/
	
	template<class a> struct is_pair : std::false_type {};
	template<class a,class b> struct is_pair<std::pair<a,b>> : std::true_type {};
	template<typename T>
	concept Pair = is_pair<std::decay_t<T>>::value;
	//////////////////////////////////////////////////////////////
	//Section: Forward declarations for serialization_string_exact_size
	//////////////////////////////////////////////////////////////
	

	//The purpose of making this a function (since it just returns the result of a concept) is to delay the evaluation of the concept
	//until all of the functions are declared.
	//
	//The function is defined in synchro.hpp
	template<typename T>
	constexpr bool SerializationStringExactSizeCompileTimeKnowable_fn();

	template<Tuple in>
	consteval bool _tuple_contents_all_compile_time_knowable()
	{
		bool result = true; 
		variadic_util::for_each_type_in_variadic<in>([&]<typename T>()
		{
			if constexpr(!SerializationStringExactSizeCompileTimeKnowable_fn<T>())
			{
				result = false;
			}
		});
		return result;
	}
	template<Snapshot in>
	consteval bool _snapshot_contents_all_compile_time_knowable()
	{
		return _tuple_contents_all_compile_time_knowable<typename in::data_t>();
	}


	
	template<typename T>
	concept ArithmeticNotEnum = std::is_arithmetic_v<T>;
	template<typename T>
	concept ArithmeticOrEnum = std::is_arithmetic_v<T> || std::is_enum_v<T>;
	template<typename T>
	concept EnumNotArithmetic = !std::is_arithmetic_v<T> || std::is_enum_v<T>;


	//forward declare everything so that they can all see each other
	constexpr size_t serialization_string_exact_size(std::monostate const &);
	template<ArithmeticOrEnum in>
	constexpr size_t serialization_string_exact_size(in const &);
	//This concept is defined after all serialization_string_exact_size overloads have been declared
	template<typename T>
	concept SerializationStringExactSizeCompileTimeKnowable = requires()
	{
		{serialization_string_exact_size<T>()} -> std::same_as<size_t>;
	};
	//and its value is accessed from a function when needed inside the overloads, because you can't prototype a concept.
	template<typename T>
	constexpr bool SerializationStringExactSizeCompileTimeKnowable_fn()
	{
		return SerializationStringExactSizeCompileTimeKnowable<T>;
	}
	template<Snapshot in>
	constexpr size_t serialization_string_exact_size(in const &);
	template<Tuple in>
	constexpr size_t serialization_string_exact_size(in const &);
	template<Container in>
	constexpr size_t serialization_string_exact_size(in const & container) ;
	template<Tuple in>
		requires (!_tuple_contents_all_compile_time_knowable<in>())
	constexpr size_t serialization_string_exact_size(in const & tuple);
	template<Optional in>
	constexpr size_t serialization_string_exact_size(in const & opt);
	template<Variant in>
	constexpr size_t serialization_string_exact_size(in const & variant);
	template<typename in>
		requires (SerializationStringExactSizeCompileTimeKnowable_fn<in>())
	constexpr size_t serialization_string_exact_size(in const &);
	template<Snapshot in>
		requires (!_snapshot_contents_all_compile_time_knowable<in>())
	constexpr size_t serialization_string_exact_size(in const & snapshot);


	//////////////////////////////////////////////////////////////
	//Section: Serialization tape class definition
	//////////////////////////////////////////////////////////////

	//this is put here so that classes which contain a serialization_tape don't have to include synchro.hpp in their header files
	struct serialization_tape
	{
		std::string data;
		size_t current_cursor = 0;
		size_t max_alloc = 2147483648; //maximum alloc that an array can request on deserialization. 
		int tape_version = 0; // may be used in the future for legacy compatibility

		serialization_tape() {}

		template<typename T>
		serialization_tape(T in);

		inline void reset_cursor()
		{
			current_cursor = 0;
		}
		inline size_t remaining_bytes_to_read()
		{
			return data.size() - current_cursor;
		}
	};
	//////////////////////////////////////////////////////////////
	//Section: ElementalSerializationType definition
	//////////////////////////////////////////////////////////////

	/*!
	 * Anything that can be directly serialized, except for `Snapshot`s.
	 *
	 * This represents `fundamental` types in Sync's eyes: these types have custom
	 * code written to serialize them for various reasons.
	 *
	 * Must also be movable and copyable.
	 */
	template<typename T>
	concept ElementalSerializationType = requires(T t, serialization_tape & tape, T && move)
	{
		{ serialize(tape,t) };
		{ deserialize<std::decay_t<T>>(tape) };
		//must overload the template below
		//TODO: finish implementing that for all default ElementalSerializationType's so I can uncomment that line
		//typename apply_template_to_inside_of_type_t<std::remove_reference_t<T>, null_template_t>;
		{ T(t) }; //must be copyable
		{ T(move) }; //must be movable
		{ !std::is_pointer_v<T> }; //cannot be a pointer (these are allowed in snapshots when properly flagged, but cannot be put in serialization tapes)
		{ !has_snapshot_enumeration<T> }; //cannot be both an elemental serialization type and have a snpashot enum
	};


	//
	//
	//
	template<typename T>
	concept ApplyTemplateToInsideIsNullOp = ArithmeticOrEnum<T> || std::is_same_v<T,std::monostate> || std::is_pointer_v<T> || Snapshot<T>;
	
	//either an int/float/non-template elemental serialization type, or a type which has a snapshot enumeration
	template<typename T>
	concept DirectType = (ApplyTemplateToInsideIsNullOp<T> || !ElementalSerializationType<T>);


	template<typename T, typename snapshot_of_T_t>
		requires (DirectType<T> && ( !Snapshot<snapshot_of_T_t> || SnapshotOf<T, snapshot_of_T_t>))
	T construct_from_snapshot(snapshot_of_T_t const & snapshot);
	template<SequenceContainer T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container);
	template<Tuple T, typename snapshot_of_T_t>
		//requires(!Pair<T>)
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container);
	template<Variant T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container);
	//template<Pair T, typename snapshot_of_T_t>
	//T construct_from_snapshot(snapshot_of_T_t const & snapshot_container);
	template<AssociativeContainer T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container);

	
	//////////////////////////////////////////////////////////////
	//Section: Essential type / macro defintions
	//////////////////////////////////////////////////////////////

	using portable_size_t = unsigned long long;

	//Enumeration defintion -- nothing really special.
	template<auto ... tuple_arg>
	class enumeration : public enumeration_base_class
	{
		public:
			std::tuple static constexpr tuple = {tuple_arg...};
	};	

#define SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT()\
	inline void operator=(synchro::Snapshot auto const & in)\
	{\
		in.apply(this);\
	}

	//////////////////////////////////////////////////////////////
	//Section: Sync flags & Sync flag helper functions
	//////////////////////////////////////////////////////////////
	template<typename T>
		requires (!SyncFlag<T>)
	constexpr auto member_ptr_behind_sync_flags(T value)
	{
		return value;
	}

	template<typename T>
		requires SyncFlag<T>
	constexpr auto member_ptr_behind_sync_flags(T value)
	{
		return member_ptr_behind_sync_flags(T::value);
	}

	//sync_flags are designed to be stackable because mutiple flags may be on a single object
	//
	//to facilitate this, the type returned by a given sync_flag is gotten by applying each flag's type modifier
	//in order.
	//
	//for example,
	//
	//sync_flag_a<sync_flag_b<member_ptr>> ->  modify_a(modify_b(value))

	template<typename T>
	auto enum_impl_value_of_flag_fn()
	{
		if constexpr(SyncFlag<T>)
		{
			return variadic_util::type_proxy<typename T::type>{};
		}
		return variadic_util::type_proxy<algorithm::remove_const_member_pointer_t<T>>{};
	}
	template<typename T>
	using impl_value_of_flag_t = typename decltype(enum_impl_value_of_flag_fn<T>())::type;
	

	//TODO fix VV
	/*
	template<auto T>
	class treat_pointer_as_reference_to_object : public sync_flag
	{
		public:
			//value is simply what the flag contains. It may be another flag, or a member pointer.
			static constexpr auto value = T;

			//Original type is:
			//(if T is a flag) decltype(T::original_type)
			//(if T is a member ptr): the type of what the member ptr points to
			//
			//Or, conceptually, it is the type that the flag will transform
			using original_type = get_original_type<decltype(T)>;

			//Type is the type after applying this flag's transformations to the type.
			using type = std::remove_pointer_t<original_type>&;

			static constexpr inline type get_translated_given_original(original_type & original)
			{
				return *original;
			}
	};
	template<typename T>
	concept ReferenceTreatedPointer = std::is_same_v<treat_pointer_as_reference_to_object<T::value>, T>;
	*/

	template<auto T>
	class treat_pointer_as_integral_type : public sync_flag
	{
		public:
			static constexpr auto value = T;

			using type = impl_value_of_flag_t<decltype(T)>;

			/*!
			 * Passed into rhs is the value which is stored (basically, the value given by
			 * get_value_of).
			 *
			 * The operations done by all sync flags will be reversed, then the data will be
			 * set.
			 */
			static inline void set_equal_to(auto* class_to_apply, auto const & rhs_snapshot, auto global_info)
			{
				//we don't actually apply any transformations
				impl_set_equal_to<value>(class_to_apply,rhs_snapshot, global_info);
			}
			/*!
			 * This will get the underlying value pointed to by the sync flag, then apply the flag's
			 * transformations. Then value returned will by stored by the Snapshot.
			 *
			 * This works recursively if sync flags are stacked.
			 */
			static inline type get_value_of(auto* class_to_sync_from, auto global_info)
			{
				//we don't actually apply any transformations
				return impl_get_value_of<value>(class_to_sync_from, global_info);
			}

			using override_no_pointers = std::true_type;
	};

	template<typename T>
	concept IntegralTreatedPointer = std::is_same_v<treat_pointer_as_integral_type<T::value>, T>;

	template<typename T, typename rhs_t, typename global_info_t>
	concept GlobalInfoRecognizingLoadFunction = requires(T t, rhs_t const & rhs,  global_info_t global_info)
	{
		{ t(rhs, global_info) };
		//std::is_same_v<std::tuple_element<1,algorithm::func_inputs_to_tuple_t<T>>,global_info_t>;
	};
	//concept must be broken into 2 parts to avoid trying to index a tuple at an out-of-bounds index
	template<typename T>
	concept GlobalInfoAndVersionIdRecognizingSaveFunction_0 = std::tuple_size_v<algorithm::func_inputs_to_tuple_t<T>> == 4;

	template<typename T>//, typename global_info_t>
	concept GlobalInfoAndVersionIdRecognizingSaveFunction_1 = requires(T t)
	{
		//the 2nd argument is global_info_t
		//std::is_same_v<std::tuple_element_t<1, algorithm::func_inputs_to_tuple_t<T>>,std::same_as<global_info_t>>;
		//the 3rd and 4th ("in" version_id and "out" version_id respectively) are of the same type.
		std::is_same_v< std::decay_t<std::tuple_element_t<2, algorithm::func_inputs_to_tuple_t<T>>> ,
		                std::decay_t<std::tuple_element_t<3, algorithm::func_inputs_to_tuple_t<T>>> >;
	};
	
	template<typename T>//, typename global_info_t>
	concept GlobalInfoAndVersionIdRecognizingSaveFunction = GlobalInfoAndVersionIdRecognizingSaveFunction_0<T> && GlobalInfoAndVersionIdRecognizingSaveFunction_1<T>;//, global_info_t>;


	template<auto save_out, auto load_in, typename _version_id_t = void>
	class sync_with_functions : public sync_flag
	{
		public:
			static constexpr auto value = load_in;

			using type = algorithm::func_return_t<decltype(save_out)>;

			using version_id_t = _version_id_t;/*typename decltype([]()
			{
				if constexpr(GlobalInfoAndVersionIdRecognizingSaveFunction<decltype(save_out)>)
				{
					return algorithm::type_proxy<std::decay_t<std::tuple_element_t<2, algorithm::func_inputs_to_tuple_t<decltype(save_out)>>>>{};
				}
				return algorithm::type_proxy<void>{};
			}())::type;*/

			static inline void set_equal_to(auto* class_to_apply, auto const & rhs_snapshot, [[maybe_unused]] auto global_info)
			{
				//if they want global_info, then give them it when we call. otherwise, don't.
				if constexpr(GlobalInfoRecognizingLoadFunction<algorithm::remove_const_member_pointer_t<decltype(load_in)>,type,decltype(global_info)>)
				{
					(class_to_apply->*load_in)(construct_from_snapshot<type>(rhs_snapshot), global_info);
				}
				else
				{
					(class_to_apply->*load_in)(construct_from_snapshot<type>(rhs_snapshot));
				}
			}

			static inline type get_value_of(auto* class_to_sync_from, [[maybe_unused]] auto global_info, auto & ... version_id_in_and_out )
			{
				if constexpr(!std::is_same_v<version_id_t,void>)//GlobalInfoAndVersionIdRecognizingSaveFunction<decltype(save_out)>)
				{
					return (class_to_sync_from->*save_out)(global_info, version_id_in_and_out...);
				}
				else
				{
					return (class_to_sync_from->*save_out)();
				}
			}
	};


	template<typename T>
	concept HasVersionId = requires
	{
		typename T::version_id_t;
	};
	
	template<typename flag_or_member_ptr_t>
	auto impl_get_version_id()
	{
		if constexpr(HasVersionId<flag_or_member_ptr_t>)
		{
			return algorithm::type_proxy<typename flag_or_member_ptr_t::version_id_t>{};
		}
		else
		{
			return algorithm::type_proxy<void>{};
		}
	}

	template<typename flag_or_member_ptr_t>
	using impl_get_version_id_t = typename decltype(impl_get_version_id<flag_or_member_ptr_t>())::type;

	template<typename lhs_t, typename rhs_t>
	concept CanUseOpEquals = requires(lhs_t & lhs, rhs_t const & rhs)
	{
		{ lhs = rhs };
	};
	template<auto flag_or_member_ptr>
	inline void impl_set_equal_to(auto* class_to_apply, auto const & rhs_snapshot, [[maybe_unused]] auto global_info)
	{
		using flag_or_member_ptr_t = decltype(flag_or_member_ptr);
		if constexpr(SyncFlag<flag_or_member_ptr_t>)
		{
			flag_or_member_ptr_t::set_equal_to(class_to_apply, rhs_snapshot, global_info);
		}
		else
		{
			using type = algorithm::remove_const_member_pointer_t<decltype(flag_or_member_ptr)>;
			if constexpr(CanUseOpEquals<std::decay_t<type>, decltype(rhs_snapshot)>)
			{
				class_to_apply->*flag_or_member_ptr = rhs_snapshot;
			}
			else
			{
				class_to_apply->*flag_or_member_ptr = construct_from_snapshot<type>(rhs_snapshot);
			}
		}
	}
	template<auto flag_or_member_ptr>
	inline auto impl_get_value_of(auto* class_to_sync_from, [[maybe_unused]] auto global_info, auto & ... version_id_in_and_out)
	{
		using flag_or_member_ptr_t = decltype(flag_or_member_ptr);
		if constexpr(SyncFlag<flag_or_member_ptr_t>)
		{
			return flag_or_member_ptr_t::get_value_of(class_to_sync_from, global_info, version_id_in_and_out...);
		}
		else
		{
			return class_to_sync_from->*flag_or_member_ptr;
		}
	}
	template<typename flag_or_member_ptr_t>
	inline constexpr auto impl_get_original_type_fn()
	{
		if constexpr(SyncFlag<flag_or_member_ptr_t>)
		{
			return impl_get_original_type_fn<decltype(flag_or_member_ptr_t::value)>();
		}
		else
		{
			return algorithm::type_proxy<algorithm::remove_const_member_pointer_t<flag_or_member_ptr_t>>{};
		}
	}
	template<typename flag_or_member_ptr_t>
	using impl_get_original_type_t = typename decltype(impl_get_original_type_fn<flag_or_member_ptr_t>())::type;

	template<typename flag_or_member_ptr_t>
	inline auto impl_get_storage_type()
	{
		if constexpr(SyncFlag<flag_or_member_ptr_t>)
		{
			return algorithm::type_proxy<typename flag_or_member_ptr_t::type>{};
		}
		else
		{
			return algorithm::type_proxy<algorithm::remove_const_member_pointer_t<flag_or_member_ptr_t>>{};
		}
	}
	template<typename flag_or_member_ptr_t>
	using impl_get_storage_type_t = typename decltype(impl_get_storage_type<flag_or_member_ptr_t>())::type;

	//use a concept here because T::override_no_pointers may not be defined and that would cause an error inside of a function
	template<typename T>
	concept internal_FlagOverridesNoPointerRule_NoRecurse = std::is_same_v<typename T::override_no_pointers, std::true_type>;

	template<typename T>
	consteval bool internal_FlagOverridesNoPointerRule_fn()
	{
		if constexpr(SyncFlag<T>)
		{
			return internal_FlagOverridesNoPointerRule_NoRecurse<T> || 
				internal_FlagOverridesNoPointerRule_fn<decltype(T::value)>();
		}
		return false;
	}

	template<typename T>
	concept FlagOverridesNoPointerRule = internal_FlagOverridesNoPointerRule_fn<T>();



	struct internal_test_class_t
	{
		int* ptr;
		int** ptr_ptr;
	} inline internal_test_class;

	inline int* some_int_ptr;

	//static_assert( std::is_same_v<decltype(member_ptr_behind_sync_flags<treat_pointer_as_integral_type<&internal_test_class_t::ptr>{}>()), int* internal_test_class_t::*>);
	//static_assert( std::is_same_v<decltype(member_ptr_behind_sync_flags<treat_pointer_as_reference_to_object<&internal_test_class_t::ptr>{}>()), int* internal_test_class_t::*>);

	static_assert( std::is_same_v<treat_pointer_as_integral_type<&internal_test_class_t::ptr>::type, int*>);
	//static_assert( std::is_same_v<treat_pointer_as_reference_to_object<&internal_test_class_t::ptr>::type, int&>);
	static_assert( std::is_same_v<impl_get_original_type_t<treat_pointer_as_integral_type<&internal_test_class_t::ptr>>, int*>);
	//static_assert( std::is_same_v<treat_pointer_as_reference_to_object<treat_pointer_as_integral_type<&internal_test_class_t::ptr_ptr>{}>::type, int*&>);

	//static_assert( std::is_same_v< 

	//obviously a single `treat_pointer_as_integral_type` flag around an int* should override the no ptr rule
	static_assert( FlagOverridesNoPointerRule< treat_pointer_as_integral_type<&internal_test_class_t::ptr>>);
	//and treating it as a reference shouldn't
	//static_assert( !FlagOverridesNoPointerRule< treat_pointer_as_reference_to_object<&internal_test_class_t::ptr>>);
	//Slightly less obvious example. This example treats the _pointer to the pointer_ as a _reference_,
	//however the pointer inside (the pointer that is being pointed to) is treated as an integral type.
	//This means that it should still override the no pointer rule.
	//static_assert( FlagOverridesNoPointerRule< treat_pointer_as_reference_to_object<treat_pointer_as_integral_type<&internal_test_class_t::ptr_ptr>{}>>);


	template<size_t i>
	inline constexpr auto get(synchro::Snapshot auto const & sync_data);
	template<auto member_ptr>
		requires( algorithm::MemberPointer<decltype(member_ptr)> )
	inline constexpr auto get(synchro::Snapshot auto const & sync_data);
//These macros can be used piecewise, e.x.
/*
		SYNCHRO_SHOULD_STORE_BEGIN(idmap)
			SYNCHRO_SHOULD_STORE(storage),
			...
			SYNCHRO_SHOULD_STORE(next_id)
		SYNCHRO_SHOULD_STORE_END()

		SYNCHRO_CONSTRUCTOR_BEGIN(idmap)
			SYNCHRO_CONSTRUCTOR(storage),
			...
			SYNCHRO_CONSTRUCTOR(next_id)
		SYNCHRO_CONSTRUCTOR_END()


	I understand these are rather difficult to understand and hides a lot of
	what's going on, but that's sort of the point. The difficulty to understand
	I blame on C++.
*/
//But it's better to just:
//		SYNCHRO(idmap,storage,...,next_id)
//Given that you're OK with the 342 limit to the number of things you can sync (A limitation of the macro, not the snapshot)
//That macro will define your enumeration, operator=, and constructor.
//Unfortunately, if you use flags then you can only partially take advantage of macros (use SHOULD_STORE and SYNCHRO_ONLY_CONSTRUCTOR)

#define SYNCHRO_SHOULD_STORE_BEGIN(this_class_name) using SYNCHRO_this_t_SHOULD_STORE = this_class_name;\
	using enumeration = synchro::enumeration \
	<
#define SYNCHRO_SHOULD_STORE(member_var) &SYNCHRO_this_t_SHOULD_STORE:: member_var
#define SYNCHRO_SHOULD_STORE_END() >; SYNCHRO_OPERATOR_EQUALS_FOR_SNAPSHOT()

#define SYNCHRO_CONSTRUCTOR_BEGIN(this_class_name) using SYNCHRO_this_t_CONSTRUCTOR= this_class_name;\
	inline constexpr this_class_name (synchro::Snapshot auto const & snapshot) : 
#define SYNCHRO_CONSTRUCTOR(member_var) member_var (synchro::get<&SYNCHRO_this_t_CONSTRUCTOR:: member_var >(snapshot))
#define SYNCHRO_CONSTRUCTOR_END() {}

#define SYNCHRO_INTERNAL_SYNCHRO__HELPER_SHOULD_STORE_0(this_class_name, ...)\

#define SYNCHRO_NO_CONSTRUCTOR(this_class_name, ...)\
	SYNCHRO_SHOULD_STORE_BEGIN(this_class_name)\
	VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_SHOULD_STORE,__VA_ARGS__)\
	SYNCHRO_SHOULD_STORE_END()
//For empty structs, we use this.
#define SYNCHRO_NOTHING()\
	using enumeration = synchro::enumeration < \
	SYNCHRO_SHOULD_STORE_END()
#define SYNCHRO_ONLY_CONSTRUCTOR(this_class_name, ...)\
	SYNCHRO_CONSTRUCTOR_BEGIN(this_class_name)\
	VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_CONSTRUCTOR,__VA_ARGS__)\
	SYNCHRO_CONSTRUCTOR_END()

	//TODO: 
	//This should = default all of the comparison operators, move, spaceship, etc.
	//Cause why not :p, we still have struct generating ones. The two interfaces can then
	//just be swapped until it works lol
#define SYNCHRO(this_class_name, ...)\
	SYNCHRO_NO_CONSTRUCTOR(this_class_name,__VA_ARGS__)\
	SYNCHRO_ONLY_CONSTRUCTOR(this_class_name,__VA_ARGS__)

#define SYNCHRO_INTERNAL_STRUCT_CTOR_ARG_HELPER(x) decltype(x) x
#define SYNCHRO_INTERNAL_STRUCT_DEFN_HELPER(x) x(x)
#define SYNCHRO_INTERNAL_STRUCT_DEFN_COPY_HELPER(x) x(other. x)
#define SYNCHRO_INTERNAL_STRUCT_DEFN_MOVE_HELPER(x) x(std::move(other. x))
#define SYNCHRO_INTERNAL_STRUCT_OPEQ_COPY_HELPER(x) x = other. x;
#define SYNCHRO_INTERNAL_STRUCT_OPEQ_MOVE_HELPER(x) x = std::move(other. x);

//Really this can be used anywhere to quick-define a struct constructor. If you need that.
#define SYNCHRO_JUST_REGULAR_CONSTRUCTOR(this_class_name, ...)\
	inline this_class_name ( VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_CTOR_ARG_HELPER, __VA_ARGS__) ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_HELPER, __VA_ARGS__)\
	{}
#define SYNCHRO_JUST_COPY_CONSTRUCTOR(this_class_name, ...)\
	inline this_class_name ( this_class_name const & other ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_COPY_HELPER, __VA_ARGS__)\
	{}	
#define SYNCHRO_JUST_MOVE_CONSTRUCTOR(this_class_name, ...)\
	inline this_class_name ( this_class_name && other ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_MOVE_HELPER, __VA_ARGS__)\
	{}	
#define SYNCHRO_JUST_COPY_EQUALS_OPERATOR(this_class_name, ...)\
	inline this_class_name & operator=( this_class_name const & other )\
	{\
		VARIADIC_UTIL_FOR_EACH(SYNCHRO_INTERNAL_STRUCT_OPEQ_COPY_HELPER, __VA_ARGS__)\
		return *this;\
	}	
#define SYNCHRO_JUST_MOVE_EQUALS_OPERATOR(this_class_name, ...)\
	inline this_class_name & operator=( this_class_name && other )\
	{\
		VARIADIC_UTIL_FOR_EACH(SYNCHRO_INTERNAL_STRUCT_OPEQ_MOVE_HELPER, __VA_ARGS__)\
		return *this;\
	}	

#define SYNCHRO_COPYMOVE(this_class_name, ...)\
	SYNCHRO(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_COPY_CONSTRUCTOR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_MOVE_CONSTRUCTOR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_COPY_EQUALS_OPERATOR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_MOVE_EQUALS_OPERATOR(this_class_name, __VA_ARGS__)

//Define all synchro constructs, and also make a regular constructor. TODO: move/copy ctor?
#define SYNCHRO_FULL(this_class_name, ...)\
	SYNCHRO_COPYMOVE(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_REGULAR_CONSTRUCTOR(this_class_name, __VA_ARGS__)

//SYNCHRO_FULL plus a default constructor
#define SYNCHRO_STRUCT(this_class_name, ...)\
	SYNCHRO_FULL(this_class_name, __VA_ARGS__)\
	inline this_class_name () {}

/////////
//The same thing, but constexpr. :/

#define SYNCHRO_JUST_REGULAR_CONSTRUCTOR_CONSTEXPR(this_class_name, ...)\
	inline constexpr this_class_name ( VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_CTOR_ARG_HELPER, __VA_ARGS__) ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_HELPER, __VA_ARGS__)\
	{}
#define SYNCHRO_JUST_COPY_CONSTRUCTOR_CONSTEXPR(this_class_name, ...)\
	inline constexpr this_class_name ( this_class_name const & other ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_COPY_HELPER, __VA_ARGS__)\
	{}	
#define SYNCHRO_JUST_MOVE_CONSTRUCTOR_CONSTEXPR(this_class_name, ...)\
	inline constexpr this_class_name ( this_class_name && other ) : \
		VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(SYNCHRO_INTERNAL_STRUCT_DEFN_MOVE_HELPER, __VA_ARGS__)\
	{}	
#define SYNCHRO_JUST_COPY_EQUALS_OPERATOR_CONSTEXPR(this_class_name, ...)\
	inline constexpr this_class_name & operator=( this_class_name const & other )\
	{\
		VARIADIC_UTIL_FOR_EACH(SYNCHRO_INTERNAL_STRUCT_OPEQ_COPY_HELPER, __VA_ARGS__)\
		return *this;\
	}	
#define SYNCHRO_JUST_MOVE_EQUALS_OPERATOR_CONSTEXPR(this_class_name, ...)\
	inline constexpr this_class_name & operator=( this_class_name && other )\
	{\
		VARIADIC_UTIL_FOR_EACH(SYNCHRO_INTERNAL_STRUCT_OPEQ_MOVE_HELPER, __VA_ARGS__)\
		return *this;\
	}	


#define SYNCHRO_COPYMOVE_CONSTEXPR(this_class_name, ...)\
	SYNCHRO(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_COPY_CONSTRUCTOR_CONSTEXPR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_MOVE_CONSTRUCTOR_CONSTEXPR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_COPY_EQUALS_OPERATOR_CONSTEXPR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_MOVE_EQUALS_OPERATOR_CONSTEXPR(this_class_name, __VA_ARGS__)

//Define all synchro constructs, and also make a regular constructor. TODO: move/copy ctor?
#define SYNCHRO_FULL_CONSTEXPR(this_class_name, ...)\
	SYNCHRO_COPYMOVE_CONSTEXPR(this_class_name, __VA_ARGS__)\
	SYNCHRO_JUST_REGULAR_CONSTRUCTOR_CONSTEXPR(this_class_name, __VA_ARGS__)

//SYNCHRO_FULL plus a default constructor
#define SYNCHRO_STRUCT_CONSTEXPR(this_class_name, ...)\
	SYNCHRO_FULL_CONSTEXPR(this_class_name, __VA_ARGS__)\
	inline constexpr this_class_name () {}


}
