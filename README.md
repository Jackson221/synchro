# Synchro Module

Synchro provides methods for synchronizing data. It is useful for syncing over a network, between threads, or for writing data to the disk.

It is written in C++20 and compiles on g++11 and later.

# Dependancies

Depends upon module3D's variadic_util module

Optionally depends upon the test module. Define NO_TEST if building without it.

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
