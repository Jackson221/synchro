/*!
 * The Sync module
 *
 * The sync module handles syncing data between places.
 * Sync is a generic library that works for writing serialized data
 * to a disk, for syncing objects between threads, for syncing objects across networks,
 * etc. 
 */
#pragma once

//Note to future readers: Work on this began in 2019, with a _draft_ standard of c++20. If things are weird
//or STL stuff is reimplemented, that's why.

#include "enumeration.hpp"

#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <variant>
#include <bit>


#include "variadic_util/variadic_util.hpp"
#ifndef NO_TEST
#include "test/forward.hpp"
#endif //NO_TEST

#if defined(__GNUC__) 
	#define PLATFORM_ITANIUM_ABI
	#include <cxxabi.h>
#endif


//Big ol' todo:
//
// * rearrange the entire order of everything in order to be more space-effecient
// 		-> this is hard :(
// 		-> also ABI breakage has to be considered when doing this.
// 			(i.e. the user might not expect that changing the size of a serialization would break ABI)
// 		-> there are two different targets for optimization: inter-thread data transfer (i.e. making the structure smaller in memory)
// 			and serialization size effeciency (basically just compacting bools to use 
//
// 	* make tuples work with user defined tuples
// 		-> requires changes on variadic_util
// 		-> note: now some of the work is done, the only thing that needs to be done is 
// 		a variadic_cat function and then tuple creation needs to be changed.
//
// 	* sync only the bits that changed..
// 		-> something like std::vector<synchro::change_tracker<T>>
//
// 	* std::array - like containers support
// 		probably change Container concept to DynamicContainer and introduce StaticContainer
//
// 	* optional "lazy-evaluated" snapshots
// 		-> would not be thread-safe, and this would be a documented shortcoming.
//
// 		-> however, it would be more effecient for use cases where snapshots are immediatly
// 		used to create a serialization tape.


namespace synchro
{
	
#ifdef PLATFORM_ITANIUM_ABI
	inline std::string internal_platform_demangle_name(const char* name_in)
	{
		char* output_buffer = reinterpret_cast<char*>(malloc(1024));
		output_buffer[0] = '\0'; //If there was a failure, this means the conversion to std::string will 
		//give an empty string
		size_t length;
		int status;

		char* result = abi::__cxa_demangle(name_in, output_buffer, &length, &status);

		std::string returnval;
		if (status == 0)
		{
			returnval = std::string(result);
		}
		free(reinterpret_cast<void*>(result));
		if (status != 0)
		{
			return std::string("[libstdc++ backend] Error code: " + std::to_string(status));
		}

		return returnval;
	}
#else 
	inline std::string internal_platform_demangle_name(const char* name_in)
	{
		return "[unsupported ABI]";
	}
#endif
	

	
	//////////////////////////////////////////////////////////////
	//Section: Serialization size-getting functions
	//////////////////////////////////////////////////////////////

	/*!
	 * Some types have sizes which can be known at compile time.
	 * These types will only define a `serialization_string_exact_size` with no parameters
	 *
	 * If the size of the object cannot be known at compile time, then their `serialization_string_exact_size` 
	 * overload will contain a single parameter which is a reference to the object to be sized.
	 *`
	 * If the first overload is defined, then the second should NOT be defined. There is a single parameter overload 
	 * for all `serialization_string_exact_size` overloads which have no parameters at the bottom of this section.
	 */

	//TODO: Remove `min size`?
	//note: min size is to be used on single-obj containers like `optional` where the user might specify that they want to
	//just assume the object is there and waste that space if it is not.
	
	
	template<ArithmeticOrEnum in>
	constexpr size_t serialization_string_min_size()
	{
		return sizeof(in);
	}
	template<Snapshot in>
	constexpr size_t serialization_string_min_size()
	{
		size_t result = 0; 
		variadic_util::for_each_type_in_variadic<typename in::data_t>([&]<typename T>()
		{
			result+=serialization_string_min_size<T>();
		});
		return result;
	}
	//TODO: ^ V Combine these?
	//Note: May not be a good idea as Snapshots may soon have special additions (like synchro::very_unlikely_to_be_present<std::optional<...>>)
	//which should be treated specially but not in tuples.
	template<Tuple in>
	constexpr size_t serialization_string_min_size()
	{
		size_t result = 0; 
		variadic_util::for_each_type_in_variadic<in>([&]<typename T>()
		{
			result+=serialization_string_min_size<T>();
		});
		return result;
	}

	//
	//
	//
	
	constexpr size_t serialization_string_exact_size(std::monostate const &)
	{
		return 0;
	}

	template<ArithmeticOrEnum in>
	constexpr size_t serialization_string_exact_size(in const &)
	{
		return serialization_string_min_size<in>();
	}
	template<EnumNotArithmetic in, ArithmeticNotEnum fin>
	constexpr size_t serialization_string_exact_size(fin const &)
	{
		return serialization_string_min_size<in>();
	}

	template<Snapshot in>
	constexpr size_t serialization_string_exact_size(in const &)
	{
		size_t result = 0; 
		variadic_util::for_each_type_in_variadic<typename in::data_t>([&]<typename T>()
		{
			result+=serialization_string_exact_size<T>();
		});
		return result;
	}
	template<Tuple in>
	constexpr size_t serialization_string_exact_size(in const &)
	{
		size_t result = 0; 
		variadic_util::for_each_type_in_variadic<in>([&]<typename T>()
		{
			result+=serialization_string_exact_size<T>();
		});
		return result;
	}
	template<Container in>
	constexpr size_t serialization_string_exact_size(in const & container) 
	{
		size_t result = 0;
		if constexpr(SerializationStringExactSizeCompileTimeKnowable_fn<typename in::value_type>()) //the container contains elements which have a fixed-size serialization string. 
		{//therefore we can just take the container size times that size and save peformance
			result = container.size()*serialization_string_exact_size<typename in::value_type>();
		}
		else //the container contains variable-size objects and each will have to be counted
		{
			for (auto & obj : container)
			{
				result += serialization_string_exact_size(obj);
			}
		}
		//factor in the size of container that is written before its contents
		return result+serialization_string_exact_size<portable_size_t>(0);
	}

	//non-compile time serialization string exact size overloads
	template<Tuple in>
		requires (!_tuple_contents_all_compile_time_knowable<in>())
	constexpr size_t serialization_string_exact_size(in const & tuple)
	{
		size_t result = 0; 
		variadic_util::for_each_element_in_variadic(tuple, [&]<typename T>(T & obj)
		{
			result+=serialization_string_exact_size<T>(obj);
		});
		return result;
	}

	template<Optional in>
	constexpr size_t serialization_string_exact_size(in const & opt)
	{
		size_t result = sizeof(bool);
		if (opt.has_value())
		{
			if constexpr(SerializationStringExactSizeCompileTimeKnowable_fn<typename in::value_type>())
			{
				result += serialization_string_exact_size<typename in::value_type>();
			}
			else
			{
				result += serialization_string_exact_size<typename in::value_type>(opt.value());
			}
		}
		return result;
	}

	template<Variant in>
	constexpr size_t serialization_string_exact_size(in const & variant)
	{
		return std::visit([](auto value)
		{
			return serialization_string_exact_size(value);
		}, variant) + serialization_string_exact_size<portable_size_t>(0);
 	}


	//allow all compile-time knowable exact sizes to also be found using the `runtime-knowable` syntax (i.e. the
	//overload where a reference to the object is passed in)
	template<typename in>
		requires (SerializationStringExactSizeCompileTimeKnowable_fn<in>())
	constexpr size_t serialization_string_exact_size(in const &)
	{
		return serialization_string_min_size<in>();
	}



	//as the very last thing, declare and define an overload for snapshots which
	//are not compile-time knowable. 
	template<Snapshot in>
		requires (!_snapshot_contents_all_compile_time_knowable<in>())
	constexpr size_t serialization_string_exact_size(in const & snapshot)
	{
		size_t result = 0; 
		variadic_util::for_each_element_in_variadic(snapshot.data, [&]<typename T>(T & snapshot_obj)
		{
			result+=serialization_string_exact_size<std::remove_const_t<T>>(snapshot_obj);
		});
		return result;
	}

	

	//End serialization_size segement

	//////////////////////////////////////////////////////////////
	//Section: basic datatypes (i.e. ints,floats,chars, etc.) serialization
	//////////////////////////////////////////////////////////////
	
	template<typename T>
	auto de_enum()
	{
		if constexpr (std::is_enum_v<T>)
		{
			return variadic_util::type_proxy<std::underlying_type_t<T>>{};
		}
		else
		{
			return variadic_util::type_proxy<T>{};
		}
	}
			
	
	//It turns out bit-shifting in generic contexts requires ugly code.
	//
	//Can be cleaned up when gcc supports bit_cast.
	//
	//Relies on processors having uniform representations (except for endianness).
	template<typename T>
	inline constexpr void serialize_basic(char* data, T in)
	{
		/*using T_unconst_maybe_bool = decltype(de_enum<std::remove_const_t<T>>())::type;
		using T_unconst = decltype(de_enum<std::conditional_t<std::is_same_v<T_unconst_maybe_bool, bool>, unsigned char, T_unconst_maybe_bool>>())::type;
		using T_unsigned = std::make_unsigned_t<T_unconst>;

		T_unsigned in_unsigned = std::bit_cast<T_unsigned>(in);
		for (unsigned long long i = 0; i < sizeof(T_unsigned); i++)
		{
			auto val = static_cast<unsigned char>(in_unsigned << (i*8));
			data[i] = *reinterpret_cast<char*>(&val);
		}*/
		auto barray = std::bit_cast<std::array<unsigned char, sizeof(T)>>(in);
		for (short i = 0; i < static_cast<short>(sizeof(T)); i++)
		{
			data[i] = barray[i];
		}
	}
	template<typename T>
	inline constexpr T deserialize_basic(char* data)
	{
		using T_unconst_maybe_bool = decltype(de_enum<std::remove_const_t<T>>())::type;
		using T_unconst = std::conditional_t<std::is_same_v<T_unconst_maybe_bool, bool>, unsigned char, T_unconst_maybe_bool>;
		using T_unsigned = std::make_unsigned_t<T_unconst>;
		T_unconst result = 0;
		for (unsigned long long i = 0; i < sizeof(T); i++)
		{
			result |= static_cast<T_unsigned>(*reinterpret_cast<unsigned char*>(data+i)) << static_cast<T_unsigned>(i*8);
		}
		return static_cast<T>(result);
	}
	
	//////////////////////////////////////////////////////////////
	//Section: Serialize functions
	//////////////////////////////////////////////////////////////
	/*!
	 * Any type where the user has defined serialization functions as member functions
	 *
	 * Note that if you can't do this, you can still define your own serialize functions
	 * that will get called due to ADL. However, it's much easier to use ExtensionSerializble
	 * if you own the code you're trying to make a custom serializer for.
	 */
	template<typename T>
	concept ExtensionSerializable = requires(T const t, serialization_tape & tape)
	{
		{ t.serialize(tape) }; 
		{ T(tape) };
	};

	//Built-in serialization functions for basic data types and STL-like types.
	template<ArithmeticOrEnum T>
	inline void serialize(serialization_tape & tape, T const & in)
	{
		if constexpr(std::is_same_v<T,float>)
		{
			serialize_basic(tape.data.data()+tape.current_cursor, std::bit_cast<std::int32_t>(in));
		}
		else if constexpr(std::is_same_v<T,double>)
		{
			serialize_basic(tape.data.data()+tape.current_cursor, std::bit_cast<std::int64_t>(in));
		}
		else
		{
			serialize_basic(tape.data.data()+tape.current_cursor, in);
		}
		tape.current_cursor += sizeof(T);
	}
	inline void serialize(serialization_tape & tape, Snapshot auto const & in)
	{
		serialize(tape, in.data); //call serialize tuple which is below VVV
	}
	inline void serialize(serialization_tape & tape, Tuple auto const & in)
	{
		variadic_util::for_each_element_in_variadic(in, [&](auto this_data)
		{
			serialize(tape,this_data);
		});
	}
	inline void serialize(serialization_tape & tape, Optional auto const & in)
	{
		//serialize whether it has a value, and then if it does the value.
		serialize(tape,in.has_value());
		if (in.has_value())
		{
			serialize(tape,in.value());
		}
	}
	inline void serialize(serialization_tape & tape, ExtensionSerializable auto const & in)
	{
		in.serialize(tape);
	}
	inline void serialize(serialization_tape & tape, Container auto const & container)
	{
		//serialize the element count and then each element
		serialize(tape, static_cast<portable_size_t>(container.size()));
		for(auto & element : container)
		{
			serialize(tape, element);	
		}
	}

	inline void serialize(serialization_tape & tape, Variant auto const & variant)
	{
		//serialize the type index inside, and then serialize the actual data
		serialize(tape, static_cast<portable_size_t>(variant.index()));
		std::visit([&](auto const & in){serialize(tape,in);},variant);
	}
	//do nothing, monostates contain no information
	inline void serialize(serialization_tape const &, std::monostate const &)
	{
	}
	/*inline void serialize(serialization_tape & tape, AssociativeContainer container)
	{
		serialize(portable_size_t>(tape, static_cast<portable_size_t>(container.size())));
		for (auto & 
	}*/


	//////////////////////////////////////////////////////////////
	//Section: Deserialization functions
	//////////////////////////////////////////////////////////////
	
	struct faulty_tape : public std::exception
	{
		std::string str_msg;
		const char* msg;
		inline faulty_tape(std::string _str_msg) :
			str_msg(_str_msg),
			msg(str_msg.c_str())
		{
		}
		inline const char* what() const noexcept
		{
			return msg;
		}
	};

	struct container_reports_size_bigger_than_remaining_tape : public faulty_tape
	{
		inline container_reports_size_bigger_than_remaining_tape(const char* type_name, size_t needed_size, size_t tape_size_remaining) :
			faulty_tape(std::string("type { ") + type_name + " } needed " + std::to_string(needed_size) + " bytes but tape only had " + std::to_string(tape_size_remaining) + " bytes left.")
		{
		}
	};
	//TODO should be running total per tape
	struct container_requests_bigger_than_max_allowed_alloc_size : public faulty_tape
	{
		inline container_requests_bigger_than_max_allowed_alloc_size(const char* type_name, size_t requested_size, size_t max_size) : 
			faulty_tape(std::string("type { ") + type_name + " } requested " + std::to_string(requested_size) + " bytes but tape specified that the max alloc was " + std::to_string(max_size) + " bytes.")
		{
		}
	};
	struct tape_requests_past_the_end_variant_type : public faulty_tape
	{
		inline tape_requests_past_the_end_variant_type(const char* type_name, size_t requested_type, size_t variant_size) : 
			faulty_tape(std::string("while deserializing variant type { ") + type_name + " }, tape requested type index " + std::to_string(requested_type) + " but variant size is " + std::to_string(variant_size))
		{
		}
	};

	//Notes about container serialization:
	//
	//A container cannot always know the serialization size of its contained elements (in case they are varying sizes).
	//
	//So, there must be two branches: one where every element is checked to see if it is greater than the remaining 'reel', 
	//and one where the reel size is checked ahead of time (more performant)
	

	
	template<ArithmeticOrEnum T, bool tape_guaranteed_big_enough = false>
	inline T deserialize(serialization_tape & tape)
	{
		if constexpr(!tape_guaranteed_big_enough)
		{
			constexpr size_t needed_size_in_bytes = serialization_string_exact_size<T>(0);
			if (needed_size_in_bytes > tape.remaining_bytes_to_read())
			{
				throw container_reports_size_bigger_than_remaining_tape(internal_platform_demangle_name(typeid(T).name()).c_str(),needed_size_in_bytes,tape.remaining_bytes_to_read());
			}
		}

		using deserialize_as = std::conditional_t
		<
			/*if  */std::is_same_v<T,float>,
			/*then*/std::int32_t    , std::conditional_t<
			/*elif*/std::is_same_v<T,double>,
			/*then*/std::int64_t,
			/*else*/T                 >
		>;

		auto return_val = std::bit_cast<T>(deserialize_basic<deserialize_as>(tape.data.data() + tape.current_cursor));
		tape.current_cursor += sizeof(T);
		return return_val;
	}

	template<ExtensionSerializable T, bool tape_guaranteed_big_enough = false>
	inline T deserialize(serialization_tape & tape)
	{
		//TODO: Possible without requiring user deduction guides?
		//TODO: Is this possible at all?
		return T:: template T<tape_guaranteed_big_enough>(tape);
	}

	template<Snapshot snapshot_t, bool tape_guaranteed_big_enough = false>
	inline snapshot_t deserialize(serialization_tape & tape)
	{
		return snapshot_t(tape);
	}
	template<Tuple tuple_t, bool tape_guaranteed_big_enough = false>
		//requires (!Pair<tuple_t>) //pairs are tuples but we can't return a tuple as a pair
	inline tuple_t deserialize(serialization_tape & tape)
	{
		return variadic_util::for_each_iterator_make_tuple<std::tuple_size_v<tuple_t>>([&]<size_t i>()
		{
			using T = std::tuple_element_t<i,tuple_t>;
			return deserialize<T,tape_guaranteed_big_enough>(tape);
		});
	}
	/*
	template<Pair pair_t, bool tape_guaranteed_big_enough = false>
	inline pair_t deserialize(serialization_tape & tape)
	{
		auto deser_0 = deserialize<std::tuple_element_t<0,pair_t>,tape_guaranteed_big_enough>(tape);
		auto deser_1 = deserialize<std::tuple_element_t<1,pair_t>,tape_guaranteed_big_enough>(tape);

		return pair_t(deser_0,deser_1);
	}
	*/
	template<Optional opt_t, bool tape_guaranteed_big_enough = false>
	inline opt_t deserialize(serialization_tape & tape)
	{
		auto has_value = deserialize<bool, tape_guaranteed_big_enough>(tape);
		if (has_value)
		{
			return opt_t(deserialize<typename opt_t::value_type, tape_guaranteed_big_enough>(tape));
		}
		return opt_t();
	}

	template<Variant variant_t, size_t i=0, bool tape_guaranteed_big_enough = false>
	inline variant_t variant_deserialize_impl(size_t const index, serialization_tape & tape)
	{
		//basically, we have the compiler generate a case for all types, and then a case for an invalid state (the exception below)
		//
		//in optimized builds this should have a similar cost to a switch statement
		if constexpr(i == variadic_util::variadic_size_v<variant_t>)
		{
			throw tape_requests_past_the_end_variant_type(internal_platform_demangle_name(typeid(variant_t).name()).c_str(),index,variadic_util::variadic_size_v<variant_t>);
		}
		else
		{
			if (index == i) //this is the type requested, return a deserialized variant
			{
				return variant_t{deserialize<variadic_util::variadic_element_t<i,variant_t>,tape_guaranteed_big_enough>(tape)};
			}
			else //keep recursing
			{
				return variant_deserialize_impl<variant_t,i+1,tape_guaranteed_big_enough>(index,tape);
			}
		}
	}

	template<Variant variant_t, bool tape_guaranteed_big_enough = false>
	inline variant_t deserialize(serialization_tape & tape)
	{
		auto index = static_cast<size_t>(deserialize<portable_size_t, tape_guaranteed_big_enough>(tape));
		return variant_deserialize_impl<variant_t>(index,tape);
	}
	template<typename monostate_t, bool = true>
		requires(std::is_same_v<monostate_t, std::monostate>)
	inline constexpr std::monostate deserialize(serialization_tape const &) 
	{
		return {};
	}

	/*!
	 * May throw std::bad_alloc, or any exceptions that the contained element's copy/move/regular constructor can throw.
	 */
	template<Container T, bool tape_guaranteed_big_enough = false>
	inline T deserialize(serialization_tape & tape)
	{
		//SAFETY CRITICAL FUNCTION
		//
		//This must be able to properly handle untrusted data. Mainly, it must avoid reading more data than what the tape holds
		//(i.e. walking off the end of the tape)

		constexpr bool content_size_compile_time_known = SerializationStringExactSizeCompileTimeKnowable<typename T::value_type>;
		auto size = deserialize<portable_size_t,tape_guaranteed_big_enough>(tape); 

		/*What exactly did this do?
		  auto needed_size_in_bytes = serialization_string_exact_size<typename T::value_type>() * size;

		if (needed_size_in_bytes > tape.max_alloc)
		{
			throw container_requests_bigger_than_max_allowed_alloc_size(internal_platform_demangle_name(typeid(T).name()).c_str(),needed_size_in_bytes,tape.max_alloc);
		}*/

		//If we can know the size of our contained types at compile time, then we check ahead-of-time if the tape is big enough to read all of them.
		if constexpr(content_size_compile_time_known && !tape_guaranteed_big_enough)
		{ 
		  	auto needed_size_in_bytes = serialization_string_exact_size<typename T::value_type>(std::declval<typename T::value_type>()) * size;
			if(needed_size_in_bytes > tape.remaining_bytes_to_read())
			{
				throw container_reports_size_bigger_than_remaining_tape(internal_platform_demangle_name(typeid(T).name()).c_str(),needed_size_in_bytes,tape.remaining_bytes_to_read());
			}
		}
		T container;
		
		if constexpr(Reservable<T>)
		{
			container.reserve(size); //may throw memory errors
		}
		for (size_t i = 0; i < size; i++)
		{
			//If we can't know the size of our contained types at compile time, then subsequent deserialize calls will check if the tape is big enough.
			if constexpr(SequenceContainer<T>)
			{
				container.push_back(deserialize<typename T::value_type, tape_guaranteed_big_enough || content_size_compile_time_known>(tape)); //may throw memory/user errors
			}
			else
			{
				container.insert(container.end(), deserialize<typename T::value_type, tape_guaranteed_big_enough || content_size_compile_time_known>(tape)); //may throw memory/user errors
			}

		}
		return container;
	}
	
	
	//////////////////////////////////////////////////////////////
	//Section: serialization tape constructor
	//////////////////////////////////////////////////////////////


	template<typename T>
	serialization_tape::serialization_tape(T in) : 
		serialization_tape()
	{
		//I would use a requires statement instead, but it would propogate back into the `prototype`
		//and cause scope issues
		static_assert(ElementalSerializationType<T> || Snapshot<T>, "data must be serializable");
		data.resize(serialization_string_exact_size<T>(in));
		serialize(*this,in);
	}

	//////////////////////////////////////////////////////////////
	//Section: Functions to get the template from the type, e.g. std::vector<int> -> std::vector
	//////////////////////////////////////////////////////////////

	template<typename T>
	concept CanUseTupleSize = requires()
	{
		typename std::tuple_size<T>;
		typename std::tuple_element_t<size_t{}, T>;
	};



	//TODO: Relocate this to some new library
	
	//These allow you to get the template type from an instantiated version, i.e:
	//std::vector<int> -> std::vector
	template<typename T, template<typename> typename container_template>
	auto internal_get_sequence_container_template_helper(container_template<typename T::value_type>)
	{
		return variadic_util::template_proxy<container_template>{};
	}

	// These two templates allow std::array - like containers to work as well. I had to split the functions into two to avoid
	// using std::tuple_size_v on types where it wasn't allowed.
	template<typename T, template<typename, size_t> typename container_template>
	auto internal_get_sequence_container_template_helper_helper(container_template<typename T::value_type, std::tuple_size_v<T>>)
	{
		return variadic_util::template_proxy_1_T_1_auto<container_template>{};
	}
	template<typename T>
		requires (CanUseTupleSize<T>) //std::tuple_element is valid on std::array
	auto internal_get_sequence_container_template_helper(auto in)
	{
		return internal_get_sequence_container_template_helper_helper<T>(in);
	}
	
	template<typename template_instantiated_container>
	using get_sequence_container_template = decltype(internal_get_sequence_container_template_helper<template_instantiated_container>(
		std::declval<template_instantiated_container>()));


	// Map-like containers:
	template<typename T, template<typename, typename> typename container_template>
	auto internal_get_map_container_template_helper(container_template<typename T::key_type, typename T::mapped_type>)
	{
		return variadic_util::template_proxy_2<container_template>{};
	}

	template<typename template_instantiated_container>
	using get_map_container_template = decltype(internal_get_map_container_template_helper<template_instantiated_container>(
		std::declval<template_instantiated_container>()));

	//////////////////////////////////////////////////////////////
	//Section: apply template to inside of type function
	//////////////////////////////////////////////////////////////
	
	//apply_template_to_inside_of_type_t<T,apply_t> returns the type after applying the apply_t template to the
	//insides of T. In other words, if T is a std::vector<Y>, then this will return std::vector<apply_t<Y>>
	//
	//Basically, it's a means of supporting templated type (like containers) that aren't written with support of synchro
	//(like the standard library) or need specialize features (like storing variable amounts of data, like std::vector)
	//
	//The way this works is by applying the snapshot to the "inside" type. So, now you have a std::vector<snapshot<T>>.
	//However, if the snapshot represented by apply_t is a resync_snapshot, then it wouldn't capture `const` data 
	//members. This is a problem, since containers are dynamic and thus we don't know if an initial_snapshot has been
	//used beforehand to capture that data. Thus, for variable size containers, we used the "decayed" version --
	//and initial_snapshot. So you will always get `std::vector<initial_snapshot<T>>` even if it's being captured
	//with a resync_snapshot.

	template<ApplyTemplateToInsideIsNullOp T, template <typename> typename /*apply_t*/, template<typename> typename /*decay_apply_t*/>
	consteval auto apply_template_to_inside_of_type()
	{
		//these types do not have any "inside type", so the template won't be applied.
		return variadic_util::type_proxy<T>{};
	}
	template<has_snapshot_enumeration T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		return variadic_util::type_proxy<apply_t<T>>{};
	}
	template<SequenceContainer T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type();
	template<AssociativeContainer T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type();
	template<typename T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
		requires (Tuple<T>)// && !Pair<T>)
	consteval auto apply_template_to_inside_of_type();
	template<Variant T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type();
	template<Optional T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type();
	//template<Pair T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	//consteval auto apply_template_to_inside_of_type();

	template<typename T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	using apply_template_to_inside_of_type_t = typename decltype(apply_template_to_inside_of_type<T,apply_t,decay_apply_t>())::type;

//These macros run apply_template_to_inside_of_type from a recursive context, the second one also decays the apply_t used.
#define APPLY(typein)         typename decltype(apply_template_to_inside_of_type< typein , apply_t,       decay_apply_t>())::type
#define APPLY_DECAYED(typein) typename decltype(apply_template_to_inside_of_type< typein , decay_apply_t, decay_apply_t>())::type

	//For the following two functions, we will apply the snapshot to the inside type of the container (e.x. T in std::vector<T>)
	//to give the result (e.x. std::vector<snapshot<T>>).
	template<SequenceContainer T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		if constexpr(CanUseTupleSize<T>) //is a std::array - like
		{
			return variadic_util::type_proxy
			<
				typename get_sequence_container_template<T>::template type
				<
					APPLY_DECAYED(typename T::value_type), std::tuple_size_v<T>
				>
			>{};
		}
		else
		{
			return variadic_util::type_proxy
			<
				typename get_sequence_container_template<T>::template type<APPLY_DECAYED(typename T::value_type)> 
			>{};
		}
	}
	template<AssociativeContainer T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		return variadic_util::type_proxy
		<
			typename get_map_container_template<T>::template type
			<
				APPLY_DECAYED(typename T::key_type), APPLY_DECAYED(typename T::mapped_type)
			>
		>{};
	}
	
	//Similar to the containers, but apply the snapshot to every type in the tuple.
	
	//Use a helper template so that I can get `apply_template_to_inside_of_type` with only the
	//first arg unspecified, which each type of the tuple will get fed into.
	template<template <typename> typename apply_t, template<typename> typename decay_apply_t>
	struct impl_helper_get_apply_template_with_apply_t_and_decay_apply_t_args_specified
	{
		template<typename T>
		using type = APPLY(T);
	};

	template<typename T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
		requires (Tuple<T>)// && !Pair<T>)
	consteval auto apply_template_to_inside_of_type()
	{
		//tuples are a constant size container, thus any `const` members are initialized upon tuple construction -- therefore, we don't need to decay
		using helper_templated = impl_helper_get_apply_template_with_apply_t_and_decay_apply_t_args_specified<apply_t, decay_apply_t>;

		return variadic_util::type_proxy<variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<helper_templated::template type, T>>{};
	}
	template<Variant T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		//variants can re-assign their contents and thus `const` members may need to be re-synchronized -- so decay the snapshot into `initial_snapshot`s always.
		//(this done here by providing both `apply_t` and `decay_apply_t` as `decay_apply_t`)
		using helper_templated = impl_helper_get_apply_template_with_apply_t_and_decay_apply_t_args_specified<decay_apply_t, decay_apply_t>;

		return variadic_util::type_proxy<variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<helper_templated::template type, T>>{};
	}

	template<Optional T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		return variadic_util::type_proxy<typename get_sequence_container_template<T>::template type
		<
			APPLY_DECAYED(typename T::value_type)
		>>{};
	}

	/*
	template<Pair T, template <typename> typename apply_t, template<typename> typename decay_apply_t>
	consteval auto apply_template_to_inside_of_type()
	{
		//we don't have to decay, for once, since this is a statically sized template
		return variadic_util::type_proxy<std::pair
		<
			APPLY(typename T::first_type),
			APPLY(typename T::second_type)
		>>{};
	}
	*/
	

#undef APPLY
#undef APPLY_DECAYED

	//////////////////////////////////////////////////////////////
	//Section: apply template to inside function
	//////////////////////////////////////////////////////////////
	
	template<ApplyTemplateToInsideIsNullOp T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		return obj;
	}

	template<typename T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
		requires (!ElementalSerializationType<T>)
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		return snapshot_t<T>(obj);
	}

	template<Tuple T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
		//requires(!Pair<T>)
	constexpr result_t inline apply_template_to_inside(T const & obj);

	template<Container T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
	constexpr result_t inline apply_template_to_inside(T const & obj);



	//Can we really say there's a different between tuple / pair in the first place? This was made cause it didn't work without it. It seems to work now.
	/*
	template<Pair T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		return std::make_pair
		(
			apply_template_to_inside<typename T::first_type, snapshot_t, decayed_snapshot_t, apply_template_to_inside_of_type_t<typename T::first_type,snapshot_t,decayed_snapshot_t>>(obj.first),
			apply_template_to_inside<typename T::second_type, snapshot_t, decayed_snapshot_t, apply_template_to_inside_of_type_t<typename T::second_type,snapshot_t,decayed_snapshot_t>>(obj.second)
		);
	}
	*/

	template<Tuple T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
		//requires(!Pair<T>)
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		auto retval = variadic_util::for_each_iterator_make_tuple<variadic_util::variadic_size_v<T>>([&]<size_t i>()
		{
			using this_t = variadic_util::variadic_element_t<i, T>;
			return apply_template_to_inside<this_t, snapshot_t, decayed_snapshot_t, apply_template_to_inside_of_type_t<this_t,snapshot_t,decayed_snapshot_t>>(std::get<i>(obj));
		});
		if constexpr(Pair<result_t>)
		{
			return static_cast<variadic_util::pair_from_variadic_t<T>>(obj);
		}
		else
		{
			return retval;
		}
	}
	template<Variant T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		return std::visit([]<typename variant_held_object_t>(variant_held_object_t const & variant_held_obj)
		{
			return result_t{
				apply_template_to_inside<variant_held_object_t, decayed_snapshot_t, decayed_snapshot_t,
					apply_template_to_inside_of_type_t<variant_held_object_t, decayed_snapshot_t, decayed_snapshot_t>>(variant_held_obj)
			};
		}, obj);
	}


	template<Container T, template<typename> typename snapshot_t, template<typename> typename decayed_snapshot_t, typename result_t>
	constexpr result_t inline apply_template_to_inside(T const & obj)
	{
		result_t result;

		if constexpr(Reservable<result_t>)
		{
			result.reserve(obj.size()); //may throw memory errors
		}
		for (auto const & item : obj)
		{
			if constexpr(SequenceContainer<T>)
			{
				using push_back_t = apply_template_to_inside_of_type_t<typename T::value_type, decayed_snapshot_t, decayed_snapshot_t>;
				result.push_back(apply_template_to_inside<std::decay_t<decltype(item)>, decayed_snapshot_t, decayed_snapshot_t, push_back_t>(item));
			}
			else
			{
				//cannot use ::value_type since it is pair< *CONST* key, mapped_type>.

				using key = typename std::decay_t<decltype(item)>::first_type;
				using value = typename std::decay_t<decltype(item)>::second_type;
				
				using pair_of_snapshot_of_key_value = std::pair<
					apply_template_to_inside_of_type_t<key, decayed_snapshot_t, decayed_snapshot_t>,
					apply_template_to_inside_of_type_t<value, decayed_snapshot_t, decayed_snapshot_t>
				>;

				//Snapshots are always moveable, as well as integral types.
				auto&& in = apply_template_to_inside<std::decay_t<decltype(item)>, decayed_snapshot_t, decayed_snapshot_t, pair_of_snapshot_of_key_value>(item);
				result.insert(result.end(), std::move(in));
			}
		}
		return result;
	}
	//////////////////////////////////////////////////////////////
	//Section: construct from snapshot functions
	//////////////////////////////////////////////////////////////
	
	template<typename T, typename snapshot_of_T_t>
	concept SnapshotConstructable = requires(snapshot_of_T_t const & snapshot)
	{
		{T(snapshot)} -> std::same_as<T>;
	};


	/*
	template<has_snapshot_enumeration T>
	consteval auto impl_remove_snapshot_fn();
	template<ElementalSerializationType T>
		requires (!Snapshot<T>)
	consteval auto impl_remove_snapshot_fn();
	template<Snapshot T>
	consteval auto impl_remove_snapshot_fn();

	template<typename T>
	using remove_snapshot_t = typename decltype(impl_remove_snapshot_fn<T>())::type;


	template<has_snapshot_enumeration T>
	consteval auto impl_remove_snapshot_fn()
	{
		return variadic_util::type_proxy<T>{};
	}
	template<Snapshot T>
	consteval auto impl_remove_snapshot_fn()
	{
		return variadic_util::type_proxy<typename T::object_t>{};//impl_remove_snapshot_fn<typename T::object_t>();
	}
	template<ElementalSerializationType T>
		requires (!Snapshot<T>)
	consteval auto impl_remove_snapshot_fn()
	{
		return apply_template_to_inside_of_type<T, remove_snapshot_t, remove_snapshot_t>();
	}
	*/



	template<typename T, typename snapshot_of_T_t>
		requires (DirectType<T> && ( !Snapshot<snapshot_of_T_t> || SnapshotOf<T, snapshot_of_T_t>))
	T construct_from_snapshot(snapshot_of_T_t const & snapshot) 
	{
		//For types constructable directly via snapshot, a common type signature is to use
		//the Snapshot concept but not constrain it specifically to a snapshot of that type.
		//
		//This means that a standard-layout type which contains another type which has a
		//constructor from Snapshot types will attempted to constructed using the host
		//type, rather than the contained. This is a workaround.

		if constexpr(SnapshotConstructable<T,snapshot_of_T_t> && !(std::is_standard_layout_v<T> && std::is_default_constructible_v<T> && Snapshot<snapshot_of_T_t>))
		{
			//defered temporary materialization / guarnteed copy elision should kick in here
			return T(snapshot);
		}
		else
		{
			auto obj = T();
			obj = snapshot;
			return obj;
		}
	}
	/*template<typename snapshot_of_T_t>
	remove_snapshot_t<snapshot_of_T_t> tmp(snapshot_of_T_t const & snapshot)
	{
		return construct_from_snapshot<remove_snapshot_t<snapshot_of_T_t>>(snapshot);
	}
	
	//attempt to use apply_template_to_inside to create construct_from_snapshot; needs work


	template<typename T, typename snapshot_of_T_t>
		requires (!DirectType<T>)
	T construct_from_snapshot(snapshot_of_T_t const & snapshot) 
	{
		return apply_template_to_inside<snapshot_of_T_t, tmp, tmp, T>(snapshot);
	}*/
	template<SequenceContainer T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container)
	{
		//update the contents in an effecient manner
		T obj_container;
		if constexpr(Reservable<T>)
		{
			obj_container.reserve(snapshot_container.size());
		}
		
		for (auto it_snapshot : snapshot_container)
		{
			obj_container.push_back(construct_from_snapshot<typename T::value_type>(it_snapshot));
		}
		return obj_container;
	}
	template<Tuple T, typename snapshot_of_T_t>
		//requires(!Pair<T>)
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container)
	{
		return variadic_util::for_each_iterator_make_tuple<variadic_util::variadic_size_v<T>>([&]<size_t i>()
		{
			return construct_from_snapshot<variadic_util::variadic_element_t<i, T>>(std::get<i>(snapshot_container));
		});
	}
	template<Variant T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container)
	{
		return std::visit([]<typename snapshot_inside_variaint_t>(snapshot_inside_variaint_t const & snapshot_inside_variaint) constexpr
		{
			//Construct, given a variant<snapshot<...>>, a variant<...>
			//
			//First, we find the index of the type in the snapshot variant -- we will convert it to the type which is at the regular variant at the same index
			//e.x. snapshot<string> -> string
			constexpr std::optional<size_t> variant_index = variadic_util::index_of_type_in_variadic<snapshot_of_T_t, snapshot_inside_variaint_t>();
			//Then, take that value and construct the original variant
			//e.x. variant<string, ...>{"some string"}
			return T{construct_from_snapshot<variadic_util::variadic_element_t<variant_index.value(), T>>(snapshot_inside_variaint)};
		}, snapshot_container);
	}
	/*
	template<Pair T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container)
	{
		return {construct_from_snapshot<typename T::first_type>(snapshot_container.first), construct_from_snapshot<typename T::second_type>(snapshot_container.second)};
	}
	*/
	template<AssociativeContainer T, typename snapshot_of_T_t>
	T construct_from_snapshot(snapshot_of_T_t const & snapshot_container)
	{
		T obj_container;

		for (auto& pair_of_snapshots : snapshot_container)
		{
			auto key = construct_from_snapshot<typename T::key_type>(pair_of_snapshots.first);
			auto value = construct_from_snapshot<typename T::mapped_type>(pair_of_snapshots.second);

			obj_container.insert(obj_container.end(), std::make_pair(key,value));
		}
		return obj_container;
	}

	//////////////////////////////////////////////////////////////
	//Section: Snapshots
	//////////////////////////////////////////////////////////////
	

	//Provide users a way of getting data out of a snapshot by specifying the member ptr of the data they want:
	template<size_t i>
	inline constexpr auto get(synchro::Snapshot auto const & sync_data)
	{
		using type = impl_get_original_type_t<std::tuple_element_t<i,decltype(sync_data.source_tuple)>>;

		return construct_from_snapshot<type>(std::get<i>(sync_data.data));
	}
	
	template<auto member_ptr>
		requires( algorithm::MemberPointer<decltype(member_ptr)> )
	inline constexpr auto get(synchro::Snapshot auto const & sync_data)
	{
		constexpr auto index = variadic_util::find_index_of( sync_data.flagless_source_tuple , member_ptr );

		static_assert(index.has_value(), "member variable must be contained within the data");

		return get<index.value()>(sync_data);
	}

	
	template<typename source_tuple, bool provide_diagnostic = false>
	constexpr auto class_is_snapshottable_fn()
	{
		//TODO recurse...
		return variadic_util::result_or([]()
		{
			return variadic_util::for_each_type_in_variadic<source_tuple>([&]<typename T>() constexpr
			{
				//get the original type behind the flags (e.g. it might be a reference if the flag where treat_pointer_as_reference_to_object
				//
				//and then remove a reference ( to allow treat_ptr_as_ref to actually work)
				using underlying_t = std::remove_reference_t<impl_get_storage_type_t<T>>;
				if constexpr(!ElementalSerializationType<underlying_t>)
				{
					if constexpr((!has_snapshot_enumeration<underlying_t> && !std::is_pointer_v<underlying_t>) ||
						(!FlagOverridesNoPointerRule<T> && std::is_pointer_v<underlying_t>))
					{
						//if they are asing for a diagnostic message, return a string, otherwise just return false
						if constexpr(provide_diagnostic)
						{
							std::string diagnostic = "`"+internal_platform_demangle_name(typeid(underlying_t).name())+"`";

							if constexpr(std::is_pointer_v<underlying_t>)
							{
								diagnostic += " is a pointer and was not wrapped in a flag such as synchro::treat_pointer_as_reference_to_object or synchro::treat_pointer_as_integral_type";
							}
							else if constexpr(!has_snapshot_enumeration<underlying_t>)
							{
								diagnostic += " does not enumerate its types to be synced and is not an ElementalSerializationType";
							}

							return diagnostic;
						}
						else
						{
							return false;
						}
					}
				}
			});
		},
		//If the above function has a `void` return, which indicates no error found:
		[]()
		{ 
			//if they want a diagnostic message, then they still want a string result
			//
			//Otherwise, they want a bool result, so give them that
			if constexpr(provide_diagnostic)
			{
				return "OK";
			}
			else
			{
				return true;
			}
		}());
	}
	/*!
	 * Gives a diagnostic message to indicate *why* your class is not snapshottable, i.e. what rules it breaks
	 *
	 * Returns "OK" if it is snapshottable
	 */
	template<typename class_t>
	std::string get_snapshottability_diagnostic()
	{
		std::string name = internal_platform_demangle_name(typeid(class_t).name());
		if constexpr(!has_snapshot_enumeration<class_t>)
		{
			return "`" + name +
				"` does not enumerate its types to be synced and is not an ElementalSerializationType";
		}
		else
		{
			return "within `"+name+"`: "+class_is_snapshottable_fn<decltype(class_t::enumeration::tuple), true>();
		}
	}

	template<typename source_tuple>
	concept class_is_snapshottable = class_is_snapshottable_fn<source_tuple>();

	struct faulty_snapshot : public std::exception
	{
		std::string str_msg;
		const char* msg;
		inline faulty_snapshot(std::string _str_msg) :
			str_msg(_str_msg),
			msg(str_msg.c_str())
		{
		}
		inline const char* what() const noexcept
		{
			return msg;
		}
	};


	template<typename _object_t, template<typename> typename source_tuple_maker, template<typename> typename decay_source_tuple_maker, typename global_info_t> 
		requires (class_is_snapshottable<decltype(source_tuple_maker<typename _object_t::enumeration>::source_tuple)>)
	struct sync_snapshot_base : public snapshot_base_class
	{
		template<typename T>
		using sync_snapshot_base_with_same_source_tuple_maker = sync_snapshot_base<T,source_tuple_maker,decay_source_tuple_maker, global_info_t>;

		template<typename T>
		using sync_snapshot_base_with_decayed_source_tuple_maker = sync_snapshot_base<T,decay_source_tuple_maker,decay_source_tuple_maker, global_info_t>;

		template<typename T>
		using apply_snapshot_t = apply_template_to_inside_of_type_t<T,sync_snapshot_base_with_same_source_tuple_maker, sync_snapshot_base_with_decayed_source_tuple_maker>;

		public:
			using object_t = _object_t;

			[[no_unique_address]] global_info_t global_info;

			using enumeration_t = typename object_t::enumeration;

			/*!
			 * the `source_tuple` contains all of the member pointers that `data_t` is derived from.
			 *
			 * for example, in resync snapshots it excludes members marked const
			 *
			 * it can be used to find the tuple index of a variable inside `buffer`, given only its corresponding member pointer.
			 */
			std::tuple static constexpr source_tuple = source_tuple_maker<enumeration_t>::source_tuple;
				//static_assert( variadic_util::variadic_size_v<decltype(source_tuple)> != 0 );

			//source_tuple, but with all data flags removed
			std::tuple static constexpr flagless_source_tuple = variadic_util::make_tuple_of_function_applied_to_tuple_contents(source_tuple, [](auto in) constexpr
			{
				using T = algorithm::remove_const_member_pointer_t<decltype(in)>;
				if constexpr(ElementalSerializationType<T> || std::is_pointer_v<T>)
				{
					return in;
				}
				else
				{
					return member_ptr_behind_sync_flags(in);
				}
			});

			using version_id_tuple_t = variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<impl_get_version_id_t, decltype(source_tuple), variadic_util::void_permitting_tuple<> >; 

			//TODO: Should this always be empty (default constructed) on initial snapshots? it may cause problems if, say, a journaling_container was inside a std::optional
			[[no_unique_address]] version_id_tuple_t version_id_tuple;
			
			using raw_data_t = variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<impl_get_storage_type_t, decltype(source_tuple)>;

			using data_t = variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<apply_snapshot_t, raw_data_t>;

			data_t data; //TODO replace with a 3rd party tuple that will more effeciently pack the data
			
		private:
			template<size_t i>
			auto get_value_of(object_t const & class_to_sync, global_info_t global_info, version_id_tuple_t previous_version_id_tuple)
			{
				if constexpr(std::is_same_v<typename version_id_tuple_t::element_t<i>, void>)
				{
					return impl_get_value_of<std::get<i>(source_tuple)>(&class_to_sync, global_info);
				}
				else
				{
					return impl_get_value_of<std::get<i>(source_tuple)>(&class_to_sync, global_info, previous_version_id_tuple.template get<i>(), version_id_tuple.template get<i>());
				}
			}
		public:

			//Take a 'snapshot' of an object. One reason you might do this is to send it over a network, down a cup-and-string, or between threads.
			inline sync_snapshot_base(object_t const & class_to_sync, global_info_t global_info, version_id_tuple_t previous_version_id_tuple = {}) :
				global_info(global_info),
				data( variadic_util::for_each_iterator_make_tuple<std::tuple_size_v<data_t>>([class_to_sync, &global_info, &previous_version_id_tuple, this]<size_t i>()
				{
					//First retreive the data from the class to sync, going through any translations that flags may apply.
					auto this_data = get_value_of<i>(class_to_sync,global_info, previous_version_id_tuple);

					using this_t = std::tuple_element_t<i,raw_data_t>;
					using this_t_data = std::tuple_element_t<i,data_t>;

					//Then store it. If it is a template like std::vector<...> then it will be stored as std::vector<snapshot<...>>
					if constexpr(std::is_pointer_v<this_t>)
					{
						return this_data;
					}
					else
					{
						return apply_template_to_inside<this_t,sync_snapshot_base_with_same_source_tuple_maker, sync_snapshot_base_with_decayed_source_tuple_maker, this_t_data>(this_data);
					}
				}))
			{
			}
			inline sync_snapshot_base(object_t const & class_to_sync)
				requires(std::is_default_constructible_v<global_info_t>) 
			:
				sync_snapshot_base(class_to_sync, {})
			{
			}
			//Create resync data from a serialization string. One reason you might do this is to load the state of an object from disk.
			inline sync_snapshot_base(serialization_tape & tape, global_info_t global_info) :
				global_info(global_info),
				data( variadic_util::for_each_iterator_make_tuple<std::tuple_size_v<data_t>>([&]<size_t i>()
				{
					//if the variable inside `data` we're trying to fill is trivial, then directly call deserialization functions
					using T = std::tuple_element_t<i,data_t>;
					if constexpr(ElementalSerializationType<T>)
					{
						return deserialize<T>(tape);
					}
					else //otherwise, construct a nested sync_snapshot_base.
					{
						return sync_snapshot_base_with_same_source_tuple_maker<T>(tape);
					}
				}))
			{
			}
			inline sync_snapshot_base(serialization_tape & tape) 
				requires(std::is_default_constructible_v<global_info_t>) 
			:
				sync_snapshot_base(tape, {})
			{
			}

		//TODO: Provide a different way of restricting this object (grab the class type from the member fn pointers?)
		/*protected:
			friend object_t;*/

			/*!
			 * Apply the data inside this snapshot to the object
			 *
			 * Most of the time, your operator=(synchro::Snapshot) can simply be a call to this function.
			 */
			void apply(object_t* class_to_apply) const
			{
				variadic_util::for_each_iterator<std::tuple_size_v<data_t>>([&]<size_t i>()
				{
					constexpr auto this_member_ptr = std::get<i>(source_tuple);
					auto& snap_data = std::get<i>(data);
					
					impl_set_equal_to<this_member_ptr>(class_to_apply, snap_data, global_info);
				});
			}
	};

	template<typename enumeration_t>
	struct initial_source_tuple_maker
	{
		std::tuple static constexpr source_tuple = enumeration_t::tuple;
	};
	/*!
	 * Snapshot passed to the class upon initial construction
	 *
	 * Essentially, it contains all data marked `const` as well as the usual data.
	 */
	template<typename object_t, typename global_info_t = std::monostate>
	using initial_snapshot = sync_snapshot_base<object_t, initial_source_tuple_maker,initial_source_tuple_maker, global_info_t>;
	
	template<typename enumeration_t>
	struct resync_source_tuple_maker
	{
		std::tuple static constexpr source_tuple = variadic_util::make_tuple_of_function_applied_to_tuple_contents(enumeration_t::tuple, [](auto in)
		{
			if constexpr ( std::is_const_v<impl_get_original_type_t<decltype(in)>> )
			{
				return variadic_util::ommit_from_tuple{};
			}
			else
			{
				return in;
			}
		});
	};
	/*!
	 * Snapshot passed to the class for re-syncing, i.e. after its initial construction
	 *
	 * Essentially, it just excludes all data marked `const`. 
	 */
	template<typename object_t, typename global_info_t = std::monostate>
	using resync_snapshot = sync_snapshot_base<object_t, resync_source_tuple_maker,initial_source_tuple_maker, global_info_t>;

#ifndef NO_TEST
	void unit_test(test::instance& inst);
#endif
}
#undef PLATFORM_ITANIUM_ABI
